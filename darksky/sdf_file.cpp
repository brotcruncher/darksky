#include "sdf_file.hpp"
#include <iostream>

namespace darksky
{
	sdf_file::sdf_file(string path)
	{
		file.open(path, ios::in | ios::beg | ios::binary);

		string line;
		string type;
		string attribute;
		string value;
		size_t space;

		getline(file, line);
		if (line.find("SDF 1.0") == string::npos) return;

		while (line.substr(1, 6) != "struct") // get parameter until structs
		{
			getline(file, line, ';');
			space = line.find('=');
			attribute = line.substr(0, space - 1);
			value = line.substr(space + 2, line.size() - space - 2);
			space = attribute.find(' ');
			type = attribute.substr(0, space);
			attribute = attribute.substr(space + 1, attribute.size() -1);
			if (type.front() == '\n') type = type.substr(1, type.size());

			if(attribute == "nhalo") header.nhalo = stol(value);
			if(attribute == "SCALE_NOW") header.SCALE_NOW = stod(value);
			if(attribute == "BOX_SIZE") header.BOX_SIZE = stod(value);
		}

		space = string::npos;

		while (space == string::npos)
		{
			getline(file, line);
			space = line.find("SDF-EOH");
		}

		start = static_cast<long>(file.tellg());

		file.seekg(0, ios::end);
		eof = file.tellg();
	}
	
	void sdf_file::read_points(long start, long count, sdf_point* buffer)
	{
		long read_start = this->start + start * sizeof(sdf_point);
		if (static_cast<long>(file.tellg()) != read_start) file.seekg(read_start, ios::beg);
		if (start + count > header.nhalo) count = header.nhalo - start;
		file.read((char*)buffer, count * sizeof(sdf_point));
	}
}