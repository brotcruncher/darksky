#include "zfp_processor.hpp"
#include <iostream>
#include "../libraries/glm/glm.hpp"
#include <vector>
#include <random>
#include "block_file.hpp"
#include <algorithm>
#include <fstream>
#include <array>

namespace darksky
{
// private

	// construct path to zfp file from index
	std::string zfp_processor::construct_path(unsigned int index)
	{
		unsigned int level1 = index / 16384;
		unsigned int level2 = level1 * 128 + (index - level1 * 16384) / 128;

		std::string index_prefix = "000";
		if (index < 10000)
		{
			if (index < 1000)
			{
				if (index < 100)
				{
					if (index < 10) index_prefix = "0000000";
					else index_prefix = "000000";
				}
				else index_prefix = "00000";
			}
			else index_prefix = "0000";
		}

		std::string level1_prefix = "000000";
		if (level1 < 10) level1_prefix = "0000000";

		std::string level2_prefix = "0000";
		if (level2 < 1000)
		{
			if (level2 < 100)
			{
				if (level2 < 10) level2_prefix = "0000000";
				else level2_prefix = "000000";
			}
			else level2_prefix = "00000";
		}

		return this->directory_path + "/" + level1_prefix + std::to_string(level1) + "/" + level2_prefix + std::to_string(level2) + "/" + index_prefix + std::to_string(index) + ".zfp";
	}

	// draw a n values out of a set of m values
	std::vector<unsigned int> zfp_processor::draw_n_from_m(unsigned int n, unsigned int m)
	{
		bool* particle_selected = (bool*)calloc(m, sizeof(bool));

		std::vector<unsigned int> selected_particles;
		selected_particles.reserve(n);

		std::default_random_engine generator;
		std::uniform_int_distribution<int> distribution(0, m - 1);

		while (selected_particles.size() < n)
		{
			unsigned int draw = distribution(generator);

			if (particle_selected[draw] == false)
			{
				selected_particles.push_back(draw);
				particle_selected[draw] = true;
			}
		}

		free(particle_selected);

		return selected_particles;
	}

// public

	// read zfp file sizes and initialize paericle buffer and working directories
	void zfp_processor::attach_sources(std::string directory_path, std::string sizes_path, std::string working_directory, unsigned int number_draws)
	{
		this->directory_path = directory_path;
		this->sizes_file.open(sizes_path, std::ios::binary | std::ios::beg);

		unsigned int file_count = 65536;
		m_sizes_buffer = (int64_t*)calloc(file_count, sizeof(uint64_t));
		this->sizes_file.seekg(0 * sizeof(int64_t));
		this->sizes_file.read((char*)m_sizes_buffer, file_count * sizeof(int64_t));

		m_max_size = 0;
		for (unsigned int i = 0; i < file_count; i++)
		{
			if (m_max_size < m_sizes_buffer[i]) m_max_size = m_sizes_buffer[i];
		}

		m_particle_buffer = (zfp_particle*)calloc(m_max_size, sizeof(zfp_particle));

		file_buffer.resize_capacity(100 * 1024 * 1024, m_max_size);

		m_working_directory = working_directory;
		m_number_draws = number_draws;
	}

	// create a lod file from a given zfp file index
	void zfp_processor::create_lod(unsigned int index)
	{
		int64_t size = m_sizes_buffer[index];

		file_buffer.attach_source(this->construct_path(index));
		file_buffer.read_particles(size, particle_buffer);

		// calculate representant

		std::vector<unsigned int> selected_particles = draw_n_from_m(100000, size);

		unsigned int level1 = index / 16384;
		unsigned int level2 = level1 * 128 + (index - level1 * 16384) / 128;

		std::ofstream lod_file;
		std::string working_directory = "H:/darksky/";
		lod_file.open(working_directory + std::to_string(level1) + "_" + std::to_string(level2) + "_l2_" + std::to_string(index) + ".lod", std::ios::binary | std::ios::out | std::ios::trunc);

		for (unsigned int& selected_particle : selected_particles)
		{
			lod_file.write((char*)&particle_buffer[selected_particle], sizeof(zfp_particle));
		}

		lod_file.close();

		std::cout << "lod " << index << " created." << std::endl;
	}

	// find the maximum velcoity of an particle in the range of first_index to last_index
	void zfp_processor::find_velocity(unsigned int first_index, unsigned int last_index)
	{
		float max_velocity = 0;

		unsigned int file_count = last_index - first_index + 1;
		int64_t* sizes_buffer = (int64_t*)calloc(file_count, sizeof(uint64_t));
		this->sizes_file.seekg(first_index * sizeof(int64_t));
		this->sizes_file.read((char*)sizes_buffer, file_count * sizeof(int64_t));

		int64_t max_size = 0;
		for (unsigned int i = 0; i < file_count; i++)
		{
			if (sizes_buffer[i] > max_size) max_size = sizes_buffer[i];
		}

		particle_buffer = (zfp_particle*)calloc(max_size, sizeof(zfp_particle));

		for(unsigned int current_index = first_index; current_index <= last_index; current_index++)
		{
			int64_t current_size = sizes_buffer[current_index - first_index];
			file_buffer.resize_capacity(100 * 1024 * 1024, current_size);
			file_buffer.attach_source(this->construct_path(current_index));
			file_buffer.read_particles(current_size, particle_buffer);

			for (unsigned int current_particle = 0; current_particle < current_size; current_particle++)
			{
				float current_velocity = glm::length(glm::vec3(particle_buffer[current_particle].r, particle_buffer[current_particle].g, particle_buffer[current_particle].b));

				if (current_velocity > max_velocity) max_velocity = current_velocity;
			}

			std::cout << "checked " << current_index - first_index + 1 << " / " << last_index - first_index + 1 << std::endl;
		}

		free(particle_buffer);

		std::cout << "max_velocity: " << max_velocity << std::endl;

		std::cin.ignore();
	}

	// reads the original sdf octree (ds14_a_1.0000) to extract subdivision for zfp files
	void zfp_processor::build_subdivision_info(std::string path)
	{
		std::ifstream octree_file;
		octree_file.open(path, std::ios::in | std::ios::beg | std::ios::binary);

		std::string line;
		size_t space;

		while (space == std::string::npos)
		{
			getline(octree_file, line);
			std::cout << line << std::endl; // REMOVE IF HARDCODING IS DONE
			space = line.find("SDF-EOH");
		}

		// skip sha ckecksums

		#pragma pack(push, 1)
		struct sha1_chunk
		{
			unsigned int sha1_len;
			unsigned char sha1[20];
		};
		#pragma pack(pop)

		octree_file.seekg(1024 * sizeof(sha1_chunk), std::ios::cur);

		size_t nodes_start = static_cast<long>(octree_file.tellg());

		// extract data for each file

		// initialize auxiliary data

		// octree data

		#pragma pack(push, 1)
		struct octree_node
		{
			int64_t base; // offset of first particle in this node
			int len; // number of particles in this node
			int index; // morton index of this node
		};
		#pragma pack(pop)

		unsigned int nodes_per_zfp = 4096;
		unsigned int bytes_per_zfp = nodes_per_zfp * sizeof(octree_node);
		unsigned int nodes_per_subblock = 64;
		unsigned int subblocks_per_zfp = 64;
		octree_node* octree_buffer = (octree_node*)malloc(bytes_per_zfp);

		// subblock data

		std::vector<zfp_subblock> subblocks(subblocks_per_zfp);

		uint64_t expected_base = 0;

		// output file with subblock information

		std::ofstream output_file;
		output_file.open("H:/darksky/zfp_subdivision.bin", std::ios::out | std::ios::beg | std::ios::binary | std::ios::trunc);

		for(unsigned int zfp_index = 0; zfp_index < 65535; zfp_index++)
		{
			octree_file.seekg(nodes_start + zfp_index * bytes_per_zfp);
			octree_file.read((char*)octree_buffer, bytes_per_zfp);

			// verify size 
			/// TODO REMOVE IF VERIFIED ONCE

			unsigned int accumulated_size = 0;
			for(unsigned int i = 0; i < nodes_per_zfp; i++)
			{
				accumulated_size += octree_buffer[i].len;
			}

			if (m_sizes_buffer[zfp_index] != accumulated_size) std::cout << "ERROR: wrong size: " << m_sizes_buffer[zfp_index] << " vs. " << accumulated_size << std::endl;

			// extract 64 subblocks for each zfp file

			unsigned int particle_offset = 0; // per zfp file

			for(unsigned int subblock = 0; subblock < 64; subblock++)
			{
				zfp_subblock& current_subblock = subblocks[subblock];

				// check if octree_nodes are coniguous

				current_subblock.first = particle_offset;
				current_subblock.length = 0;

				for(unsigned int node_index = subblock * nodes_per_subblock; node_index < subblock * nodes_per_subblock + nodes_per_subblock; node_index++)
				{
					octree_node& current_node = octree_buffer[node_index];

					if (current_node.base != expected_base) std::cout << "ERROR: unexpected base" << expected_base << " vs. " << current_node.base << std::endl;
				
					current_subblock.length += current_node.len;

					particle_offset += current_node.len;
					expected_base += current_node.len;
				}

				std::cout << "file:" << zfp_index << " subblock:" << subblock << " first:" << current_subblock.first << " length:" << current_subblock.length << std::endl;
			}

			output_file.write((char*)subblocks.data(), subblocks_per_zfp * sizeof(zfp_subblock));

			// write subblock data to output
		}

		output_file.close();
	}

	// check if all level 2 l2 .lod files are availabe
	void zfp_processor::check_level2_l2_availability()
	{
		std::string working_directory = "H:/darksky/";

		std::cout << "Checking l2 availability.";

		for (unsigned int index = 0; index < 65536; index++)
		{
			unsigned int level_1 = index / 16384;
			unsigned int level_2 = level_1 * 128 + (index - level_1 * 16384) / 128;

			std::ifstream probe(working_directory + std::to_string(level_1) + "_" + std::to_string(level_2) + "_l2_" + std::to_string(index) + ".lod");
			if (!probe.good())
			{
				std::cout << "ERROR: file is missing: " << std::to_string(level_1) + "_" + std::to_string(level_2) + "_l2_" + std::to_string(index) + ".lod" << std::endl;
				exit(-1);
			}
			if (index % 655 == 0) std::cout << ".";
			if (index % 6553 == 0) std::cout << index / 65535.f * 100.f << "% ";
		}

		std::cout << "All l2 files available." << std::endl;
	}

	// if all level 2 l2 .lod files are available they are merged to higher level lod files l1 and l0
	void zfp_processor::level2_lod_summit(unsigned int level_1)
	{
		std::string working_directory = "H:/darksky/";
		const unsigned int number_draws = 100000;
		zfp_particle* zfp_buffer = (zfp_particle*)malloc(sizeof(zfp_particle) * number_draws * 8); // working memory: 8 x number_draws

		for (unsigned int level_2 = level_1 * 128; level_2 < level_1 * 128 + 128; level_2++) // 00000000 - 00000128 
		{
			// level_2 l1 assembly
			for (unsigned int l1 = level_2 * 16; l1 < level_2 * 16 + 16; l1++)
			{
				// std::cout << "processed ";

				std::string last = working_directory + "0_0_l2_0.lod";

				// read l2 lod files
				unsigned int offset = 0;
				for (unsigned int l2 = l1 * 8; l2 < l1 * 8 + 8; l2++)
				{
					std::ifstream l2_file;

					std::string path = working_directory + std::to_string(level_1) + "_" + std::to_string(level_2) + "_l2_" + std::to_string(l2) + ".lod";
					//if (lods_equal(last, path)) std::cout << "[EQUAL LOD ERROR]->";
					last = path;

					l2_file.open(path, std::ios::beg | std::ios::binary);
					l2_file.read((char*)(zfp_buffer + offset), sizeof(zfp_particle) * number_draws);
					offset += number_draws;
					l2_file.close();

					// std::cout << std::to_string(level_1) + "_" + std::to_string(level_2) + "_l2_" + std::to_string(l2) + " ... ";
				}

				// sample number_draws particles

				std::vector<unsigned int> selected_particles = draw_n_from_m(number_draws, number_draws * 8);

				// write l1 lod file

				std::ofstream l1_file;
				l1_file.open(working_directory + std::to_string(level_1) + "_" + std::to_string(level_2) + "_l1_" + std::to_string(l1) + ".lod", std::ios::binary | std::ios::out | std::ios::trunc);

				for (unsigned int& selected_particle : selected_particles)
				{
					l1_file.write((char*)&zfp_buffer[selected_particle], sizeof(zfp_particle));
				}
				l1_file.close();

				// std::cout << "to create " << std::to_string(level_1) + "_" + std::to_string(level_2) + "_l1_" + std::to_string(l1) << std::endl;
			}

			// level 2 l0 assembly
			for (unsigned int l0 = level_2 * 2; l0 < level_2 * 2 + 2; l0++)
			{
				// std::cout << "processed ";

				std::string last = working_directory + "0_0_l2_0.lod";

				// read l1 lod files
				unsigned int offset = 0;
				for (unsigned int l1 = l0 * 8; l1 < l0 * 8 + 8; l1++)
				{
					std::ifstream l1_file;

					std::string path = working_directory + std::to_string(level_1) + "_" + std::to_string(level_2) + "_l1_" + std::to_string(l1) + ".lod";
					//if (lods_equal(last, path)) std::cout << "[EQUAL LOD ERROR]->";
					last = path;

					l1_file.open(path, std::ios::beg | std::ios::binary);
					l1_file.read((char*)(zfp_buffer + offset), sizeof(zfp_particle) * number_draws);
					offset += number_draws;
					l1_file.close();

					//std::cout << std::to_string(level_1) + "_" + std::to_string(level_2) + "_l1_" + std::to_string(l1) + " ... ";
				}
				// sample number_draws particles

				std::vector<unsigned int> selected_particles = draw_n_from_m(number_draws, number_draws * 8);

				// write l0 lod file

				std::ofstream l1_file;
				std::string path = working_directory + std::to_string(level_1) + "_" + std::to_string(level_2) + "_l0_" + std::to_string(l0) + ".lod";

				l1_file.open(path, std::ios::binary | std::ios::out | std::ios::trunc);

				for (unsigned int& selected_particle : selected_particles)
				{
					l1_file.write((char*)&zfp_buffer[selected_particle], sizeof(zfp_particle));
				}
				l1_file.close();

				//std::cout << "to create " << std::to_string(level_1) + "_" + std::to_string(level_2) + "_l0_" + std::to_string(l0) << std::endl;
			}

			// std::cout << std::endl;

			std::cout << level_2 << " / 128" << std::endl;
		}
	

		free(zfp_buffer);
	}

	// check if all level 2 l0 .lod files are valid (no duplicates) 
	void zfp_processor::verify_level2(unsigned int level_1)
	{
		std::string working_directory = "H:/darksky/";

		std::string last = working_directory + "0_0_l2_0.lod";

		bool error = false;

		for (unsigned int level_2 = level_1 * 128; level_2 < level_1 * 128 + 128; level_2++)
		{
			for (unsigned int l0 = level_2 * 2; l0 < level_2 * 2 + 2; l0++)
			{
				std::string path = working_directory + std::to_string(level_1) + "_" + std::to_string(level_2) + "_l0_" + std::to_string(l0) + ".lod";
				
				if (lods_equal(last, path))
				{
					std::cout << "[ERROR] level_2:" << level_2 << " l0:" << l0 << " results equal." << std::endl;
					error = true;
				}

				last = path;
			}
		}

		if(error == false) std::cout << "No equal lods found." << std::endl;
	}
	
	// if all level 2 l0 .lod files are available they are merged to higher level lod files level 1 l1 and l0
	void zfp_processor::level1_lod_summit(unsigned int level_1)
	{
		std::string working_directory = "H:/darksky/";
		const unsigned int number_draws = 100000;
		zfp_particle* zfp_buffer = (zfp_particle*)malloc(sizeof(zfp_particle) * number_draws * 8);	// working memory: 8 x number_draws

		for (unsigned int l1 = level_1 * 32; l1 < level_1 * 32 + 32; l1++) // 32 x 8 (128 folder, each with two level 2 l0 files)
		{
			std::cout << "processed ";

			// read l2 lod files
			unsigned int offset = 0;
			for (unsigned int level_2 = l1 * 4; level_2 < l1 * 4 + 4; level_2++) // 4 folders per l1 lod
			{
				for (unsigned int l0 = level_2 * 2; l0 < level_2 * 2 + 2; l0++) // 2 lod files from each of the 4 folders
				{
					std::ifstream level_2_l0_file;
					std::string path = working_directory + std::to_string(level_1) + "_" + std::to_string(level_2) + "_l0_" + std::to_string(l0) + ".lod";

					level_2_l0_file.open(path, std::ios::beg | std::ios::binary);
					level_2_l0_file.read((char*)(zfp_buffer + offset), sizeof(zfp_particle) * number_draws);
					offset += number_draws;
					level_2_l0_file.close();

					std::cout << std::to_string(level_1) + "_" + std::to_string(level_2) + "_l0_" + std::to_string(l0) + " ... ";
				}
			}

			// sample number_draws particles

			std::vector<unsigned int> selected_particles = draw_n_from_m(number_draws, number_draws * 8);

			// write l1 lod file

			std::ofstream l1_file;
			l1_file.open(working_directory + std::to_string(level_1) + "_l1_" + std::to_string(l1) + ".lod", std::ios::binary | std::ios::out | std::ios::trunc);

			for (unsigned int& selected_particle : selected_particles)
			{
				l1_file.write((char*)&zfp_buffer[selected_particle], sizeof(zfp_particle));
			}
			l1_file.close();

			std::cout << "to create " << std::to_string(level_1) + "_l1_" + std::to_string(l1) << std::endl;
		}

		for (unsigned int l0 = level_1 * 4; l0 < level_1 * 4 + 4; l0++) // 4 l0 files per level_1 folder
		{
			std::cout << "processed ";

			unsigned int offset = 0;
			for (unsigned int l1 = l0 * 8; l1 < l0 * 8 + 8; l1++) 
			{
					std::ifstream level_1_l1_file;
					std::string path = working_directory + std::to_string(level_1) + "_l1_" + std::to_string(l1) + ".lod";
					level_1_l1_file.open(path, std::ios::beg | std::ios::binary);
					level_1_l1_file.read((char*)(zfp_buffer + offset), sizeof(zfp_particle) * number_draws);
					offset += number_draws;
					level_1_l1_file.close();

					std::cout << std::to_string(level_1) + "_l1_" + std::to_string(l1) + " ... ";
			}

			// sample number_draws particles

			std::vector<unsigned int> selected_particles = draw_n_from_m(number_draws, number_draws * 8);

			// write l0 lod file

			std::ofstream l0_file;
			l0_file.open(working_directory + std::to_string(level_1) + "_l0_" + std::to_string(l0) + ".lod", std::ios::binary | std::ios::out | std::ios::trunc);

			for (unsigned int& selected_particle : selected_particles)
			{
				l0_file.write((char*)&zfp_buffer[selected_particle], sizeof(zfp_particle));
			}
			l0_file.close();

			std::cout << "to create " << std::to_string(level_1) + "_l0_" + std::to_string(l0) << std::endl;
		}

		free(zfp_buffer);
	}
	
	// check if all level 1 l0 .lod files are valid (no duplicates)
	void zfp_processor::verify_level1(unsigned int level_1)
	{
		std::string working_directory = "H:/darksky/";

		std::string last = working_directory + "0_0_l2_0.lod";

		bool error = false;

		for (unsigned int l0 = level_1 * 4; l0 < level_1 * 4 + 4; l0++) // 4 l0 files per level_1 folder
		{
			std::string path = working_directory + std::to_string(level_1) + "_l0_" + std::to_string(l0) + ".lod";

			if (lods_equal(last, path))
			{
				std::cout << "[ERROR] level_1:" << " l0:" << l0 << " results equal." << std::endl;
				error = true;
			}

			last = path;
		}

		if (error == false) std::cout << "No equal lods found." << std::endl;
	}

	// if all level 1 l0 .lod files are available they are merged to root level lod files level 0 l1 and l0
	void zfp_processor::level0_lod_summit()
	{
		std::string working_directory = "H:/darksky/";
		const unsigned int number_draws = 100000;
		zfp_particle* zfp_buffer = (zfp_particle*)malloc(sizeof(zfp_particle) * number_draws * 8);	// working memory: 8 x number_draws

		for (unsigned int l1 = 0; l1 < 2; l1++)
		{
			unsigned int offset = 0;

			for(unsigned int level_1 = l1 * 2; level_1 < l1 * 2 + 2; level_1++)
			{
				for(unsigned int l0 = level_1 * 4; l0 < level_1 * 4 + 4; l0++)
				{
					std::ifstream level_1_l0_file;
					std::string path = working_directory + std::to_string(level_1) + "_l0_" + std::to_string(l0) + ".lod";
					level_1_l0_file.open(path, std::ios::beg | std::ios::binary);
					level_1_l0_file.read((char*)(zfp_buffer + offset), sizeof(zfp_particle) * number_draws);
					offset += number_draws;
					level_1_l0_file.close();
				}
			}

			// sample number_draws particles

			std::vector<unsigned int> selected_particles = draw_n_from_m(number_draws, number_draws * 8);

			// write l1 lod file

			std::ofstream l1_file;
			l1_file.open(working_directory + "l1_" + std::to_string(l1) + ".lod", std::ios::binary | std::ios::out | std::ios::trunc);

			for (unsigned int& selected_particle : selected_particles)
			{
				l1_file.write((char*)&zfp_buffer[selected_particle], sizeof(zfp_particle));
			}
			l1_file.close();
		}

		// assemble root from l1_0 && l1_1

		std::ifstream first_level_0_l1_file;
		std::string first_path = working_directory + "l1_" + std::to_string(0) + ".lod";
		first_level_0_l1_file.open(first_path, std::ios::beg | std::ios::binary);
		first_level_0_l1_file.read((char*)(zfp_buffer), sizeof(zfp_particle) * number_draws);
		first_level_0_l1_file.close();

		std::ifstream second_level_0_l1_file;
		std::string second_path = working_directory + "l1_" + std::to_string(1) + ".lod";
		second_level_0_l1_file.open(second_path, std::ios::beg | std::ios::binary);
		second_level_0_l1_file.read((char*)(zfp_buffer + number_draws), sizeof(zfp_particle) * number_draws);
		second_level_0_l1_file.close();

		std::vector<unsigned int> selected_particles = draw_n_from_m(number_draws, number_draws * 2);

		std::ofstream l0_file;
		l0_file.open(working_directory + "l0_" + std::to_string(0) + ".lod", std::ios::binary | std::ios::out | std::ios::trunc);

		for (unsigned int& selected_particle : selected_particles)
		{
			l0_file.write((char*)&zfp_buffer[selected_particle], sizeof(zfp_particle));
		}
		l0_file.close();

		free(zfp_buffer);
	}

	// extract geometry info (vec4 with centroid and diameter) for subblock in zfp files
	void zfp_processor::build_subdivision_geometry()
	{
		std::ofstream output_file;
		output_file.open("H:/geometry_subblocks.bin", std::ios::beg | std::ios::binary | std::ios::trunc);

		unsigned int number_draws = 100000;
		zfp_particle* lod_buffer = (zfp_particle*)malloc(number_draws * sizeof(zfp_particle));

		for (unsigned int index = 0; index < 65536; index++)
		{
			unsigned int level1 = index / 16384;
			unsigned int level2 = level1 * 128 + (index - level1 * 16384) / 128;

			std::ifstream lod_file;
			std::string working_directory = "H:/darksky/";
			lod_file.open(working_directory + std::to_string(level1) + "_" + std::to_string(level2) + "_l2_" + std::to_string(index) + ".lod", std::ios::binary | std::ios::beg);
			lod_file.read((char*)lod_buffer, number_draws * sizeof(zfp_particle));

			glm::vec3 min(100000000.f), max(-100000000.f);

			for (unsigned int i = 0; i < number_draws; i++)
			{
				zfp_particle& current_particle = lod_buffer[i];
				if (current_particle.x < min.x) min.x = current_particle.x;
				if (current_particle.x > max.x) max.x = current_particle.x;
				if (current_particle.y < min.y) min.y = current_particle.y;
				if (current_particle.y > max.y) max.y = current_particle.y;
				if (current_particle.z < min.z) min.z = current_particle.z;
				if (current_particle.z > max.z) max.z = current_particle.z;
			}

			float step = glm::max(glm::abs(min.x - max.x), glm::max(glm::abs(min.y - max.y), glm::abs(min.z - max.z))) / 4.f;
			float half_step = step / 2.f;

			glm::vec4 geometry;
			geometry.a = step;

			for (unsigned int z_out = 0; z_out < 2; z_out++)
			{
				for (unsigned int y_out = 0; y_out < 2; y_out++)
				{
					for (unsigned int x_out = 0; x_out < 2; x_out++)
					{
						for (unsigned int z_in = 0; z_in < 2; z_in++)
						{
							for (unsigned int y_in = 0; y_in < 2; y_in++)
							{
								for (unsigned int x_in = 0; x_in < 2; x_in++)
								{
									geometry.x = min.x + x_out * step * 2 + x_in * step + half_step;
									geometry.y = min.y + y_out * step * 2 + y_in * step + half_step;
									geometry.z = min.z + z_out * step * 2 + z_in * step + half_step;

									unsigned int subblock = z_out * 32 + y_out * 16 + x_out * 8 + z_in * 4 + y_in * 2 + x_in;
									if(subblock == 63)
									{
										bool passed = true;
										if (geometry.x > max.x || geometry.x + step < max.x) passed = false;
										if (geometry.y > max.y || geometry.y + step < max.y) passed = false; 
										if (geometry.z > max.z || geometry.z + step < max.z) passed = false;
										if (!passed) std::cout << "[ERROR] build_subdivision_geometry (file " << index << ")" << std::endl;
									}

									//std::cout << std::fixed << "file:" << index << " subblock:" << z_out * 32 + y_out * 16 + x_out * 8 + z_in * 4 + y_in * 2 + x_in << " x:" << geometry.x << " y:" << geometry.y << " z:" << geometry.z << " dia:" << geometry.a << std::endl;

									output_file.write((char*)&geometry, sizeof(glm::vec4));
								}
							}
						}
					}
				}
			}

			std::cout << std::fixed << "file: " << index << " / 65536" << std::endl;
		}

		free(lod_buffer);
		
		output_file.close();
	}

	// DEPRECATED TEST FUCTION (created a block file from one .lod file)
	void zfp_processor::create_blockfile_from_lod(std::string input_path, std::string output_path, float scaling)
	{
		unsigned int number_draws = 100000;
		float max_velocity = 8315.23f;

		// write block file

		std::ofstream block_file;
		block_file.open(output_path, std::ios::binary | std::ios::out | std::ios::trunc);

		{
			bool type = false;
			unsigned int block_count = 1;
			block_file.write((char*)&type, sizeof(bool));
			block_file.write((char*)&block_count, sizeof(unsigned int));
		}

		unsigned int calculated_file_size = sizeof(bool) + sizeof(unsigned int);
		unsigned int first_particle = 0;
		unsigned int first_child = 0;
		unsigned int child_count = 0;

		// write block info

		block_file.write((char*)&child_count, sizeof(unsigned int));
		block_file.write((char*)&first_child, sizeof(unsigned int));

		block_file.write((char*)&number_draws, sizeof(unsigned int));
		block_file.write((char*)&first_particle, sizeof(unsigned int));

		// write geometry info
		glm::vec4 geometry = glm::vec4(0, 0, 0, 10.f);
		block_file.write((char*)&geometry, sizeof(glm::vec4));

		// write particles

		#pragma pack(push, 1)
		struct
		{
			glm::vec4 geometry;
			glm::uint color;
		} particle;
		#pragma pack(pop)

		std::ifstream lod_file;
		lod_file.open(input_path, std::ios::binary);
		zfp_particle* lod_buffer = (zfp_particle*)malloc(sizeof(zfp_particle) * number_draws);
		lod_file.read((char*)lod_buffer, sizeof(zfp_particle) * number_draws);
		lod_file.close();

		for (unsigned int i = 0; i < number_draws; i++)
		{
			particle.geometry.x = lod_buffer[i].x;
			particle.geometry.y = lod_buffer[i].y;
			particle.geometry.z = lod_buffer[i].z;
			particle.geometry.a = 1000.f * scaling;

			float velocity = glm::length(glm::vec3(lod_buffer[i].r, lod_buffer[i].g, lod_buffer[i].b)) / max_velocity;
			unsigned int g = glm::max((velocity - 0.25f) * 255 * 1.5f, 0.f);
			unsigned int b = glm::max((0.25f - velocity) * 255 * 4, 0.f);
			unsigned int r = 85;
			unsigned int a = velocity * 255;

			particle.color = (r << 24) + (g << 16) + (b << 8) + a;

			// std::cout << std::fixed << "x:" << particle.geometry.x << " y:" << particle.geometry.y << " z:" << particle.geometry.z << std::endl;

			block_file.write((char*)&particle, sizeof(particle));
		}

		block_file.close();

		free(lod_buffer);
	}
	
	// checks if two .lod files are equal in concern of their point values and order
	bool zfp_processor::lods_equal(std::string first, std::string second)
	{
		const unsigned int number_draws = 100000;
		zfp_particle* zfp_buffer = (zfp_particle*)malloc(sizeof(zfp_particle) * number_draws * 2);

		std::ifstream first_file;
		first_file.open(first, std::ios::beg | std::ios::binary);
		first_file.read((char*)(zfp_buffer), sizeof(zfp_particle) * number_draws);
		first_file.close();

		std::ifstream second_file;
		second_file.open(second, std::ios::beg | std::ios::binary);
		second_file.read((char*)(zfp_buffer + number_draws), sizeof(zfp_particle) * number_draws);
		second_file.close();

		glm::vec3 first_min(100000000.f), second_min(100000000.f), first_max(-100000000.f), second_max(-100000000.f);

		bool all_equal = true;

		for(unsigned int i = 0; i < number_draws; i++)
		{
			zfp_particle& first_particle = zfp_buffer[i];
			zfp_particle& second_particle = zfp_buffer[i + number_draws];

			if (first_particle.x != second_particle.x || first_particle.y != second_particle.y || first_particle.z != second_particle.z) all_equal = false;

			/*
			if (first_particle.x < first_min.x) first_min.x = first_particle.x;
			if (first_particle.y < first_min.y) first_min.y = first_particle.y;
			if (first_particle.z < first_min.z) first_min.z = first_particle.z;
			if (first_particle.x > first_max.x) first_max.x = first_particle.x;
			if (first_particle.y > first_max.y) first_max.y = first_particle.y;
			if (first_particle.z > first_max.z) first_max.z = first_particle.z;

			if (second_particle.x < second_min.x) second_min.x = second_particle.x;
			if (second_particle.y < second_min.y) second_min.y = second_particle.y;
			if (second_particle.z < second_min.z) second_min.z = second_particle.z;
			if (second_particle.x > second_max.x) second_max.x = second_particle.x;
			if (second_particle.y > second_max.y) second_max.y = second_particle.y;
			if (second_particle.z > second_max.z) second_max.z = second_particle.z;
			*/
		}

	//	std::cout << std::fixed << "first_min: " << first_min.x << " " << first_min.y << " " << first_min.z << std::endl;
	//	std::cout << std::fixed << "first_max: " << first_max.x << " " << first_max.y << " " << first_max.z << std::endl;
	//	std::cout << std::fixed << "second_min: " << second_min.x << " " << second_min.y << " " << second_min.z << std::endl;
	//	std::cout << std::fixed << "second_max: " << second_max.x << " " << second_max.y << " " << second_max.z << std::endl;

		free(zfp_buffer);

		return all_equal;
	}

	// extract geometry info (vec4 with centroid and diameter) for blocks from root to level 4 
	void zfp_processor::extract_geometry_to_1024()
	{
		std::string working_directory = "H:/darksky/";
		const unsigned int number_draws = 100000;

		glm::vec4 geometry;

		std::ofstream output_file;
		output_file.open("H:/geometry_1024.bin", std::ios::out | std::ios::beg | std::ios::binary | std::ios::trunc);

		zfp_particle* zfp_buffer = (zfp_particle*)malloc(sizeof(zfp_particle) * number_draws);

		std::string path;

		// 1 = level 0 l0 = root
		path = working_directory + "l0_0.lod";
		geometry = get_lod_geometry(path, zfp_buffer);
		output_file.write((char*)&geometry, sizeof(glm::vec4));

		std::cout << "1.. ";

		// 2 = level 0 l1
		for(unsigned int l1 = 0; l1 < 2; l1++)
		{
			path = working_directory + "l1_" + std::to_string(l1) + ".lod";
			geometry = get_lod_geometry(path, zfp_buffer);
			output_file.write((char*)&geometry, sizeof(glm::vec4));
		}

		std::cout << "2.. ";

		// 16 = level 1 l0
		for(unsigned int level_1 = 0; level_1 < 4; level_1++)
		{
			for (unsigned int l0 = level_1 * 4; l0 < level_1 * 4 + 4; l0++)
			{
				path = working_directory + std::to_string(level_1) + "_l0_" + std::to_string(l0) + ".lod";
				geometry = get_lod_geometry(path, zfp_buffer);
				output_file.write((char*)&geometry, sizeof(glm::vec4));
			}
		}

		std::cout << "16.. ";

		// 128 = level 1 l1
		for (unsigned int level_1 = 0; level_1 < 4; level_1++)
		{
			for (unsigned int l1 = level_1 * 32; l1 < level_1 * 32 + 32; l1++)
			{
				path = working_directory + std::to_string(level_1) + "_l1_" + std::to_string(l1) + ".lod";
				geometry = get_lod_geometry(path, zfp_buffer);
				output_file.write((char*)&geometry, sizeof(glm::vec4));
			}
		}

		std::cout << "128.. ";

		// 1024 = level 2 l0
		for (unsigned int level_1 = 0; level_1 < 4; level_1++)
		{
			for(unsigned int level_2 = level_1 * 128; level_2 < level_1 * 128 + 128; level_2++)
			{
				for (unsigned int l0 = level_2 * 2; l0 < level_2 * 2 + 2; l0++)
				{
					path = working_directory + std::to_string(level_1) + "_" + std::to_string(level_2) + "_l0_" + std::to_string(l0) + ".lod";
					geometry = get_lod_geometry(path, zfp_buffer);
					output_file.write((char*)&geometry, sizeof(glm::vec4));
				}
			}
			std::cout << ".";
		}

		std::cout << "1024.. ";

		output_file.close();
	}

	// extract geometry info (vec4 with centroid and diameter) per level 1 folder for blocks on level 5;
	void zfp_processor::extract_geometry_8192(unsigned int level_1)
	{
		std::string working_directory = "H:/darksky/";
		const unsigned int number_draws = 100000;

		glm::vec4 geometry;

		std::ofstream output_file;
		output_file.open("H:/geometry_8192_" + std::to_string(level_1) + ".bin", std::ios::out | std::ios::beg | std::ios::binary | std::ios::trunc);

		zfp_particle* zfp_buffer = (zfp_particle*)malloc(sizeof(zfp_particle) * number_draws);

		std::string path;

		for (unsigned int level_2 = level_1 * 128; level_2 < level_1 * 128 + 128; level_2++)
		{
			for (unsigned int l1 = level_2 * 16; l1 < level_2 * 16 + 16; l1++)
			{
				path = working_directory + std::to_string(level_1) + "_" + std::to_string(level_2) + "_l1_" + std::to_string(l1) + ".lod";
				geometry = get_lod_geometry(path, zfp_buffer);
				output_file.write((char*)&geometry, sizeof(glm::vec4));
			}
			std::cout << ".";
		}

		output_file.close();
	}

	// extract geometry info (vec4 with centroid and diameter) per level 1 folder for blocks on level 6;
	void zfp_processor::extract_geometry_65536(unsigned int level_1)
	{
		std::string working_directory = "H:/darksky/";
		const unsigned int number_draws = 100000;

		glm::vec4 geometry;

		std::ofstream output_file;
		output_file.open("H:/geometry_65536_" + std::to_string(level_1) + ".bin", std::ios::out | std::ios::beg | std::ios::binary | std::ios::trunc);

		zfp_particle* zfp_buffer = (zfp_particle*)malloc(sizeof(zfp_particle) * number_draws);

		std::string path;

		// 65536 = level 2 l2

		for (unsigned int level_2 = level_1 * 128; level_2 < level_1 * 128 + 128; level_2++)
		{
			for (unsigned int l2 = level_2 * 128; l2 < level_2 * 128 + 128; l2++)
			{
				path = working_directory + std::to_string(level_1) + "_" + std::to_string(level_2) + "_l2_" + std::to_string(l2) + ".lod";
				geometry = get_lod_geometry(path, zfp_buffer);
				output_file.write((char*)&geometry, sizeof(glm::vec4));
			}
			std::cout << ".";
		}

		output_file.close();
	}

	glm::vec4 zfp_processor::get_lod_geometry(std::string lod_path, zfp_particle* zfp_buffer)
	{
		const unsigned int number_draws = 100000;

		std::ifstream lod_file;
		lod_file.open(lod_path, std::ios::beg | std::ios::binary);

		if (!lod_file.good()) std::cout << "ERROR: file not found." << std::endl;

		lod_file.read((char*)(zfp_buffer), sizeof(zfp_particle) * number_draws);
		lod_file.close();

		glm::vec3 min = glm::vec3(100000000.f);
		glm::vec3 max = glm::vec3(-100000000.f);

		for (unsigned int i = 0; i < number_draws - 101; i += 100)
		{
			zfp_particle& current_particle = zfp_buffer[i];

			if (current_particle.x < min.x) min.x = current_particle.x;
			if (current_particle.x > max.x) max.x = current_particle.x;
			if (current_particle.y < min.y) min.y = current_particle.y;
			if (current_particle.y > max.y) max.y = current_particle.y;
			if (current_particle.z < min.z) min.z = current_particle.z;
			if (current_particle.z > max.z) max.z = current_particle.z;
		}

		glm::vec4 geometry;
		geometry.x = (min.x + max.x) / 2.f;
		geometry.y = (min.y + max.y) / 2.f;
		geometry.z = (min.z + max.z) / 2.f;
		geometry.a = glm::max(glm::abs(min.x - max.x), glm::max(glm::abs(min.y - max.y), glm::abs(min.z - max.z)));

		return geometry;
	}

	void zfp_processor::assemble_geometry()
	{
		std::ofstream output_file;
		output_file.open("H:/geometry.bin", std::ios::out | std::ios::beg | std::ios::binary | std::ios::trunc);

		glm::vec4* geometry_buffer = (glm::vec4*)malloc(sizeof(glm::vec4) * 65536 * 64);

		std::ifstream input_file;
		input_file.open("H:/geometry_1024.bin", std::ios::beg | std::ios::binary);
		input_file.read((char*)geometry_buffer, sizeof(glm::vec4) * (1024 + 128 + 16 + 2 + 1));
		input_file.close();
		output_file.write((char*)geometry_buffer, sizeof(glm::vec4) * (1024 + 128 + 16 + 2 + 1));

		input_file.open("H:/geometry_8192_0.bin", std::ios::beg | std::ios::binary);
		input_file.read((char*)geometry_buffer, sizeof(glm::vec4) * 2048);
		input_file.close();
		output_file.write((char*)geometry_buffer, sizeof(glm::vec4) * 2048);

		input_file.open("H:/geometry_8192_1.bin", std::ios::beg | std::ios::binary);
		input_file.read((char*)geometry_buffer, sizeof(glm::vec4) * 2048);
		input_file.close();
		output_file.write((char*)geometry_buffer, sizeof(glm::vec4) * 2048);

		input_file.open("H:/geometry_8192_2.bin", std::ios::beg | std::ios::binary);
		input_file.read((char*)geometry_buffer, sizeof(glm::vec4) * 2048);
		input_file.close();
		output_file.write((char*)geometry_buffer, sizeof(glm::vec4) * 2048);

		input_file.open("H:/geometry_8192_3.bin", std::ios::beg | std::ios::binary);
		input_file.read((char*)geometry_buffer, sizeof(glm::vec4) * 2048);
		input_file.close();
		output_file.write((char*)geometry_buffer, sizeof(glm::vec4) * 2048);

		input_file.open("H:/geometry_65536_0.bin", std::ios::beg | std::ios::binary);
		input_file.read((char*)geometry_buffer, sizeof(glm::vec4) * 16384);
		input_file.close();
		output_file.write((char*)geometry_buffer, sizeof(glm::vec4) * 16384);

		input_file.open("H:/geometry_65536_1.bin", std::ios::beg | std::ios::binary);
		input_file.read((char*)geometry_buffer, sizeof(glm::vec4) * 16384);
		input_file.close();
		output_file.write((char*)geometry_buffer, sizeof(glm::vec4) * 16384);

		input_file.open("H:/geometry_65536_2.bin", std::ios::beg | std::ios::binary);
		input_file.read((char*)geometry_buffer, sizeof(glm::vec4) * 16384);
		input_file.close();
		output_file.write((char*)geometry_buffer, sizeof(glm::vec4) * 16384);

		input_file.open("H:/geometry_65536_3.bin", std::ios::beg | std::ios::binary);
		input_file.read((char*)geometry_buffer, sizeof(glm::vec4) * 16384);
		input_file.close();
		output_file.write((char*)geometry_buffer, sizeof(glm::vec4) * 16384);

		// zfp subblock geometry

		input_file.open("H:/geometry_subblocks.bin", std::ios::beg | std::ios::binary);
		input_file.read((char*)geometry_buffer, sizeof(glm::vec4) * 65536 * 64);
		input_file.close();
		output_file.write((char*)geometry_buffer, sizeof(glm::vec4) * 65536 * 64);

		output_file.close();

		free(geometry_buffer);
	}

	void zfp_processor::verify_geometry_file(std::string path, unsigned int size)
	{
		glm::vec4* geometry_buffer = (glm::vec4*)malloc(sizeof(glm::vec4) * size);

		std::ifstream input_file;
		input_file.open(path, std::ios::beg | std::ios::binary);

		if (!input_file.good())
		{
			std::cout << "[ERROR] verify_geometry_file: cannot open geometry file " << path << std::endl;
			return;
		}

		input_file.read((char*)geometry_buffer, sizeof(glm::vec4) * size);
		
		bool passed = true;
		unsigned int strikes = 0;
		unsigned int last;
		for(unsigned int i = 0; i < size && passed; i++)
		{
			if (geometry_buffer[i].x + geometry_buffer[i].y + geometry_buffer[i].z + geometry_buffer[i].a == 0.f)
			{
				passed = false;
				last = i;
			}
			else if(geometry_buffer[i].x == geometry_buffer[i].y && geometry_buffer[i].y == geometry_buffer[i].z)
			{
				strikes++;
				if (strikes == 10)
				{
					passed = false;
					last = i;
				}
			}
		}

		if(passed) std::cout << path << " [VALID] (" << strikes << ")" << std::endl;
		else std::cout << path <<  "[ERROR] (" << strikes << ", " << last << ")" << std::endl;

		free(geometry_buffer);
	}

	/*
	/	construct final block file from source filed:
	/	- subdivision file (from build_subdivision_info)
	/	- geometry file (from assemble_geometry)
	/	- particle files (from assemble_particle...)
	*/
	void zfp_processor::block_file_assembly(std::string block_file_path)
	{
		// calc block info for all blocks (block_count = 8192 + 1024 + 128 + 16 + 2 + 1 = 9363 | zfp_count : 65536)

		std::ofstream block_file;
		block_file.open(block_file_path, std::ios::beg | std::ios::binary | std::ios::trunc);

		unsigned int block_count = 1 + 2 + 16 + 128 + 1024 + 8192 + 65536;
		unsigned int zfp_count = 65536;

		block_file.write((char*)&block_count, sizeof(unsigned int)); // 1. number of blocks
		block_file.write((char*)&zfp_count, sizeof(unsigned int)); // 2. number of zfp files

		// 3. block_info for each block

		block_file::block block_info;	
	
		unsigned int child_counter = 1;
		unsigned int particle_counter = 0;

		std::cout << "block info...";
		// 1
		std::cout << "1..";
		block_info.child_count = 2;
		block_info.first_child = child_counter;
		block_info.first_particle = particle_counter;
		block_info.particle_count = 100000;

		block_file.write((char*)&block_info, sizeof(block_info));

		child_counter += block_info.child_count;
		particle_counter += block_info.particle_count;
		
		// 2
		std::cout << "2..";
		for(unsigned int i = 0; i < 2; i++)
		{
			block_info.child_count = 8;
			block_info.first_child = child_counter;
			block_info.first_particle = particle_counter;
			block_info.particle_count = 100000;

			block_file.write((char*)&block_info, sizeof(block_info));

			child_counter += block_info.child_count;
			particle_counter += block_info.particle_count;
		}

		// 16
		std::cout << "16..";
		for (unsigned int i = 0; i < 16; i++)
		{
			block_info.child_count = 8;
			block_info.first_child = child_counter;
			block_info.first_particle = particle_counter;
			block_info.particle_count = 100000;

			block_file.write((char*)&block_info, sizeof(block_info));

			child_counter += block_info.child_count;
			particle_counter += block_info.particle_count;
		}

		// 128
		std::cout << "128..";
		for (unsigned int i = 0; i < 128; i++)
		{
			block_info.child_count = 8;
			block_info.first_child = child_counter;
			block_info.first_particle = particle_counter;
			block_info.particle_count = 100000;

			block_file.write((char*)&block_info, sizeof(block_info));

			child_counter += block_info.child_count;
			particle_counter += block_info.particle_count;
		}

		// 1024
		std::cout << "1024..";
		for (unsigned int i = 0; i < 1024; i++)
		{
			block_info.child_count = 8;
			block_info.first_child = child_counter;
			block_info.first_particle = particle_counter;
			block_info.particle_count = 100000;

			block_file.write((char*)&block_info, sizeof(block_info));

			child_counter += block_info.child_count;
			particle_counter += block_info.particle_count;
		}

		// 8192
		std::cout << "8192..";
		for (unsigned int i = 0; i < 8192; i++)
		{
			block_info.child_count = 8;
			block_info.first_child = child_counter;
			block_info.first_particle = particle_counter;
			block_info.particle_count = 100000;

			block_file.write((char*)&block_info, sizeof(block_info));

			child_counter += block_info.child_count;
			particle_counter += block_info.particle_count;
		}

		// 65536
		std::cout << "65536..";
		for (unsigned int i = 0; i < 65536; i++)
		{
			block_info.child_count = 0;
			block_info.first_child = i;
			block_info.first_particle = 0;
			block_info.particle_count = m_sizes_buffer[i];

			block_file.write((char*)&block_info, sizeof(block_info));
		}

		// subdivision info for all zfp files
		uint64_t bytes = sizeof(zfp_subblock) * 64 * 65536;
		std::cout << std::endl << "subdivision info...";
		zfp_subblock* subdivision_buffer = (zfp_subblock*)malloc(bytes);
		std::ifstream subdivision_file;
		subdivision_file.open("//visus.uni-stuttgart.de/visusstore/home/richtemn/Desktop/zfp_subdivision.bin", std::ios::binary | std::ios::beg);
		if (!subdivision_file.good()) std::cout << "[ERROR] cannot open subdivision file.";
		subdivision_file.read((char*)subdivision_buffer, bytes);
		subdivision_file.close();
		block_file.write((char*)subdivision_buffer, bytes);
		free(subdivision_buffer);
		
		// geometry data for all blocks
		bytes = sizeof(glm::vec4) * (1 + 2 + 16 + 128 + 1024 + 8192 + 65536 + 65536 * 64);
		std::cout << std::endl << "geometry (" << bytes << " bytes)...";
		glm::vec4* geometry_buffer = (glm::vec4*)malloc(bytes);
		std::ifstream geometry_file;
		geometry_file.open("H:/geometry.bin", std::ios::binary | std::ios::beg);
		if (!geometry_file.good()) std::cout << "[ERROR] cannot open geometry file.";
		geometry_file.read((char*)geometry_buffer, bytes);
		geometry_file.close();
		block_file.write((char*)geometry_buffer, bytes);
		free(geometry_buffer);

		// lod particle data for all blocks
		std::cout << std::endl << "particle data...";
		unsigned int number_draws = 100000;

		block_file::particle* part_buffer = (block_file::particle*)malloc(16384 * number_draws * sizeof(block_file::particle));
		std::ifstream part_file;

		std::cout << "1024...";

		part_file.open("H:/particle_data_1024.bin", std::ios::beg | std::ios::binary);
		if (!part_file.good()) std::cout << "[ERROR] cannot open particle_data_1024 file.";
		bytes = sizeof(block_file::particle) * number_draws * (1024 + 128 + 16 + 2 + 1);
		part_file.read((char*)part_buffer, bytes);
		part_file.close();
		block_file.write((char*)part_buffer, bytes);

		std::cout << "8192...0...";
		part_file.open("H:/particle_data_8192_0.bin", std::ios::beg | std::ios::binary);
		if (!part_file.good()) std::cout << "[ERROR] cannot open particle_data_8192_0 file.";
		bytes = sizeof(block_file::particle) * number_draws * 2048;
		part_file.read((char*)part_buffer, bytes);
		part_file.close();
		block_file.write((char*)part_buffer, bytes);

		std::cout << "1...";
		part_file.open("H:/particle_data_8192_1.bin", std::ios::beg | std::ios::binary);
		if (!part_file.good()) std::cout << "[ERROR] cannot open particle_data_8192_1 file.";
		part_file.read((char*)part_buffer, bytes);
		part_file.close();
		block_file.write((char*)part_buffer, bytes);

		std::cout << "2...";
		part_file.open("H:/particle_data_8192_2.bin", std::ios::beg | std::ios::binary);
		if (!part_file.good()) std::cout << "[ERROR] cannot open particle_data_8192_2 file.";
		part_file.read((char*)part_buffer, bytes);
		part_file.close();
		block_file.write((char*)part_buffer, bytes);

		std::cout << "3...";
		part_file.open("H:/particle_data_8192_3.bin", std::ios::beg | std::ios::binary);
		if (!part_file.good()) std::cout << "[ERROR] cannot open particle_data_8192_3 file.";
		part_file.read((char*)part_buffer, bytes);
		part_file.close();
		block_file.write((char*)part_buffer, bytes);

		std::cout << "65536...0...";
		part_file.open("H:/particle_data_65536_0.bin", std::ios::beg | std::ios::binary);
		if (!part_file.good()) std::cout << "[ERROR] cannot open particle_data_65536_0 file.";
		bytes = sizeof(block_file::particle) * number_draws * 16384;
		part_file.read((char*)part_buffer, bytes);
		part_file.close();
		block_file.write((char*)part_buffer, bytes);

		std::cout << "1...";
		part_file.open("H:/particle_data_65536_1.bin", std::ios::beg | std::ios::binary);
		if (!part_file.good()) std::cout << "[ERROR] cannot open particle_data_65536_1 file.";
		part_file.read((char*)part_buffer, bytes);
		part_file.close();
		block_file.write((char*)part_buffer, bytes);

		std::cout << "2...";
		part_file.open("H:/particle_data_65536_2.bin", std::ios::beg | std::ios::binary);
		if (!part_file.good()) std::cout << "[ERROR] cannot open particle_data_65536_2 file.";
		part_file.read((char*)part_buffer, bytes);
		part_file.close();
		block_file.write((char*)part_buffer, bytes);

		std::cout << "3...";
		part_file.open("H:/particle_data_65536_3.bin", std::ios::beg | std::ios::binary);
		if (!part_file.good()) std::cout << "[ERROR] cannot open particle_data_65536_3 file.";
		part_file.read((char*)part_buffer, bytes);
		part_file.close();
		block_file.write((char*)part_buffer, bytes);

		free(part_buffer);

		block_file.close();
	}

	//	convert particle from zfp to blockfile format
	void zfp_processor::convert_particles(zfp_particle* zfp_buffer, block_file::particle* part_buffer, float scaling, float alpha_scaling)
	{
		for(unsigned int i = 0; i < 100000; i++)
		{
			zfp_particle& current_z_particle = zfp_buffer[i];

			// std::cout << "x:" << zfp_buffer[i].x << " y:" << zfp_buffer[i].y << " z:" << zfp_buffer[i].z << std::endl;

			block_file::particle& current_b_particle = part_buffer[i];

			current_b_particle.geometry.x = current_z_particle.x;
			current_b_particle.geometry.y = current_z_particle.y;
			current_b_particle.geometry.z = current_z_particle.z;
			current_b_particle.geometry.a = 1000.f * scaling;

			float velocity = glm::length(glm::vec3(current_z_particle.r, current_z_particle.g, current_z_particle.b)) / 8315.23f; // hardcoded max velocity
			unsigned int g = glm::max((velocity - 0.25f) * 255 * 1.5f, 0.f);
			unsigned int b = glm::max((0.25f - velocity) * 255 * 4, 0.f);
			unsigned int r = 85;
			unsigned int a = velocity * 255;

			current_b_particle.color = (r << 24) + (g << 16) + (b << 8) + a;
		}
	}
	
	// build one particle file for all blocks from root to layer 4 (which 1024 blocks in size)
	void zfp_processor::assemble_particle_data_1024()
	{
		unsigned int number_draws = 100000;
		std::string working_directory = "H:/darksky/";

		std::ofstream particle_1024_file;
		particle_1024_file.open("H:/particle_data_1024.bin", std::ios::out | std::ios::beg | std::ios::binary | std::ios::trunc);

		uint64_t bytes = sizeof(zfp_particle) * number_draws;
		zfp_particle* zfp_buffer = (zfp_particle*)malloc(bytes);
		unsigned int part_bytes = sizeof(block_file::particle) * number_draws;
		block_file::particle* part_buffer = (block_file::particle*)malloc(part_bytes);
	
		std::ifstream lod_file;

		float scaling_65536 = 7.f; // sqrt(5000000 / 100000) -> area of 5M particles represented by 100K particles
		float scaling_8192 = scaling_65536 * sqrt(8);
		float scaling_1024 = scaling_8192 * sqrt(8);
		float scaling_128 = scaling_1024 * sqrt(8);
		float scaling_16 = scaling_128 * sqrt(8);
		float scaling_2 = scaling_16 * sqrt(8);
		float scaling_1 = scaling_2 * sqrt(2);

		// 1
		std::cout << "1.. ";
		lod_file.open(working_directory + "l0_0.lod", std::ios::beg | std::ios::binary);
		lod_file.read((char*)zfp_buffer, bytes);
		lod_file.close();
		convert_particles(zfp_buffer, part_buffer, scaling_1, 0.f);
		particle_1024_file.write((char*)part_buffer, part_bytes);

		//2
		std::cout << "2.. ";
		for (unsigned int l1 = 0; l1 < 2; l1++)
		{
			lod_file.open(working_directory + "l1_" + std::to_string(l1) + ".lod", std::ios::beg | std::ios::binary);
			lod_file.read((char*)zfp_buffer, bytes);
			lod_file.close();
			convert_particles(zfp_buffer, part_buffer, scaling_2, 0.f);
			particle_1024_file.write((char*)part_buffer, part_bytes);
		}

		// 16 = level 1 l0
		std::cout << "16.. ";
		for (unsigned int level_1 = 0; level_1 < 4; level_1++)
		{
			for (unsigned int l0 = level_1 * 4; l0 < level_1 * 4 + 4; l0++)
			{
				lod_file.open(working_directory + std::to_string(level_1) + "_l0_" + std::to_string(l0) + ".lod", std::ios::beg | std::ios::binary);
				lod_file.read((char*)zfp_buffer, bytes);
				lod_file.close();
				convert_particles(zfp_buffer, part_buffer, scaling_16, 0.f);
				particle_1024_file.write((char*)part_buffer, part_bytes);
			}
		}

		// 128 = level 1 l1
		std::cout << "128.. ";
		for (unsigned int level_1 = 0; level_1 < 4; level_1++)
		{
			for (unsigned int l1 = level_1 * 32; l1 < level_1 * 32 + 32; l1++)
			{
				lod_file.open(working_directory + std::to_string(level_1) + "_l1_" + std::to_string(l1) + ".lod", std::ios::beg | std::ios::binary);
				lod_file.read((char*)zfp_buffer, bytes);
				lod_file.close();
				convert_particles(zfp_buffer, part_buffer, scaling_128, 0.f);
				particle_1024_file.write((char*)part_buffer, part_bytes);
			}
		}

		// 1024 = level 2 l0
		std::cout << "1024.. ";
		for (unsigned int level_1 = 0; level_1 < 4; level_1++)
		{
			for (unsigned int level_2 = level_1 * 128; level_2 < level_1 * 128 + 128; level_2++)
			{
				for (unsigned int l0 = level_2 * 2; l0 < level_2 * 2 + 2; l0++)
				{
					lod_file.open(working_directory + std::to_string(level_1) + "_" + std::to_string(level_2) + "_l0_" + std::to_string(l0) + ".lod", std::ios::beg | std::ios::binary);
					particle_1024_file.write((char*)part_buffer, part_bytes);
				}
				std::cout << level_2 - level_1 * 128 << " / " << 128 << std::endl;
			}
		}

		free(zfp_buffer);
		free(part_buffer);
	}

	// build one particle file per level 1 folder for blocks on layer 5 (8192 blocks in size)
	void zfp_processor::assemble_particle_data_8192(unsigned int level_1)
	{
		unsigned int number_draws = 100000;
		std::string working_directory = "H:/darksky/";

		std::ofstream particle_8192_file;
		particle_8192_file.open("H:/particle_data_8192_" + std::to_string(level_1) + ".bin", std::ios::out | std::ios::beg | std::ios::binary | std::ios::trunc);

		uint64_t bytes = sizeof(zfp_particle) * number_draws;
		zfp_particle* zfp_buffer = (zfp_particle*)malloc(bytes);
		unsigned int part_bytes = sizeof(block_file::particle) * number_draws;
		block_file::particle* part_buffer = (block_file::particle*)malloc(part_bytes);

		std::ifstream lod_file;

		float scaling_65536 = 7.f; // sqrt(5000000 / 100000) -> area of 5M particles represented by 100K particles
		float scaling_8192 = scaling_65536 * sqrt(8);

		for (unsigned int level_2 = level_1 * 128; level_2 < level_1 * 128 + 128; level_2++)
		{
			for (unsigned int l1 = level_2 * 16; l1 < level_2 * 16 + 16; l1++)
			{
				lod_file.open(working_directory + std::to_string(level_1) + "_" + std::to_string(level_2) + "_l1_" + std::to_string(l1) + ".lod", std::ios::beg | std::ios::binary);
				lod_file.read((char*)zfp_buffer, bytes);
				lod_file.close();
				convert_particles(zfp_buffer, part_buffer, scaling_8192, 0.f);
				particle_8192_file.write((char*)part_buffer, part_bytes);
			}

			std::cout << level_2 - level_1 * 128 << " / " << 128 << std::endl;
		}
		particle_8192_file.close();

		free(zfp_buffer);
		free(part_buffer);
	}

	// build one particle file per level 1 folder for blocks on layer 6 (65536 blocks in size)
	void zfp_processor::assemble_particle_data_65536(unsigned int level_1)
	{
		unsigned int number_draws = 100000;
		std::string working_directory = "H:/darksky/";

		std::ofstream particle_65536_file;
		particle_65536_file.open("H:/particle_data_65536_" + std::to_string(level_1) + ".bin", std::ios::out | std::ios::beg | std::ios::binary | std::ios::trunc);

		uint64_t bytes = sizeof(zfp_particle) * number_draws;
		zfp_particle* zfp_buffer = (zfp_particle*)malloc(bytes);
		unsigned int part_bytes = sizeof(block_file::particle) * number_draws;
		block_file::particle* part_buffer = (block_file::particle*)malloc(part_bytes);

		std::ifstream lod_file;

		float scaling_65536 = 7.f; // sqrt(5000000 / 100000) -> area of 5M particles represented by 100K particles

		for (unsigned int level_2 = level_1 * 128; level_2 < level_1 * 128 + 128; level_2++)
		{
			for (unsigned int l2 = level_2 * 128; l2 < level_2 * 128 + 128; l2++)
			{
				lod_file.open(working_directory + std::to_string(level_1) + "_" + std::to_string(level_2) + "_l2_" + std::to_string(l2) + ".lod", std::ios::beg | std::ios::binary);
				lod_file.read((char*)zfp_buffer, bytes);
				lod_file.close();
				convert_particles(zfp_buffer, part_buffer, scaling_65536, 0.f);
				particle_65536_file.write((char*)part_buffer, part_bytes);
			}
			std::cout << level_2 - level_1 * 128 << " / " << 128 << std::endl;
		}
		
		particle_65536_file.close();

		free(zfp_buffer);
		free(part_buffer);
	}

	void zfp_processor::verify_particle_data(std::string path, uint64_t size)
	{
		int64_t bytes = sizeof(block_file::particle) * size;
		block_file::particle* part_buffer = (block_file::particle*)malloc(bytes);

		std::ifstream particle_file;
		particle_file.open(path, std::ios::binary | std::ios::beg);
		particle_file.read((char*)part_buffer, bytes);
		particle_file.close();

		bool passed = true;
		int64_t last;
		int64_t counter = 0;

		for(unsigned int i = 0; i < size && counter < 1000 && passed; i++)
		{
			if (part_buffer[i].geometry.x + part_buffer[i].geometry.y + part_buffer[i].geometry.z == 0.f)
			{
				last = i;
				counter++;
				if (counter == 1000) passed = false;
			}
			//std::cout << i << ") " << part_buffer[i].geometry.x << " " << part_buffer[i].geometry.y << " " << part_buffer[i].geometry.z << std::endl;
		}

		if (passed) std::cout << path << " [VALID] (" << counter << ")" << std::endl;
		else std::cout << path << " [ERROR] (" << counter << ", " << last << ")" << std::endl;

		free(part_buffer);
	}

	void zfp_processor::verify_block_file(std::string path) // TODO
	{
		bool passed = true;

		std::ifstream block_file;
		block_file.open(path, std::ios::binary | std::ios::beg);

		unsigned int lod_block_count;
		unsigned int zfp_file_count;

		block_file.read((char*)&lod_block_count, sizeof(unsigned int));
		block_file.read((char*)&zfp_file_count, sizeof(unsigned int));

		uint64_t bytes = lod_block_count * sizeof(block_file::block);
		block_file::block* block_info = (block_file::block*)malloc(bytes);
		block_file.read((char*)block_info, bytes);

		for(unsigned int block = 0; block < lod_block_count; block++)
		{
		}

		free(block_info);

		if (!passed) std::cout << path << " [ERROR]";
	}
}