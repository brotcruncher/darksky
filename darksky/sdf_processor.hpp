#pragma once
#include "sdf_file.hpp"
#include "../libraries/glm/glm.hpp"
#include <vector>

namespace darksky
{
	class sdf_preprocessor
	{
		public:

		sdf_preprocessor();
		void attach_source(std::string source);
		void create_blocks(std::string path, unsigned int target_size);

		private:

		sdf_file* source_file;
	};
}