#include "zfp_processor.hpp"

/*
int main()
{
	darksky::zfp_processor z;
	z.attach_sources("//CARIMBO/darksky", "//visus.uni-stuttgart.de/visusstore/home/richtemn/Desktop/octreeStats_6.bin", "H:/darksky/", 100000);

	// the entire workflow of block_file generation:

	for (unsigned int i = 0; i < 65536; i++)
	{
		z.create_lod(i);
	}

	z.check_level2_l2_availability();

	level2_lod_summit(0); // 00000000
	level2_lod_summit(1); // 00000001
	level2_lod_summit(2); // 00000002
	level2_lod_summit(3); // 00000003

	z.verify_level2(0);
	z.verify_level2(1);
	z.verify_level2(2);
	z.verify_level2(3);

	level1_lod_summit(0); // 00000000
	level1_lod_summit(1); // 00000001
	level1_lod_summit(2); // 00000002
	level1_lod_summit(3); // 00000003

	z.verify_level1(0);
	z.verify_level1(1);
	z.verify_level1(2);
	z.verify_level1(3);

	level0_lod_summit();



	// shorter running examples on subdata

	// find velocity in range 0 - 15
	z.find_velocity(0, 15); 

	// create an lod file from index 123
	z.create_lod(123);

	// assemble final block
	//z.block_file_assembly("H:/final.block");

	z.check_level2_l2_availability();

	// after creating .lod file for each zfp file, this sequence will create the entire .lod hierarchy base files
	// Note that different level_1 values can be executed in parallel (eg. multiple level2_lod_summit())

	z.level2_lod_summit(0);
	z.verify_level2(0);
	z.assemble_particle_data_8192(0);
	z.verify_particle_data("H:/particle_data_8192_3.bin", 204800000);
	z.assemble_particle_data_8192()

	z.level2_lod_summit(1);
	z.verify_level2(1);

	z.level2_lod_summit(2);
	z.verify_level2(2);

	z.level2_lod_summit(3);
	z.verify_level2(3);

	z.level1_lod_summit(0);
	z.verify_level1(0);

	z.level1_lod_summit(1);
	z.verify_level1(1);

	z.level1_lod_summit(2);
	z.verify_level1(2);

	z.level1_lod_summit(3);
	z.verify_level1(3);

	z.level0_lod_summit();

	// This assembles the particle data files
	z.assemble_particle_data_1024();
	z.assemble_particle_data_8192(0);
	z.assemble_particle_data_8192(1);
	z.assemble_particle_data_8192(2);
	z.assemble_particle_data_8192(3);
	z.assemble_particle_data_65536(0);
	z.assemble_particle_data_65536(1);
	z.assemble_particle_data_65536(2);
	z.assemble_particle_data_65536(3);

	z.verify_particle_data("H:/particle_data_8192_0.bin", 204800000);

	z.verify_geometry_file("H:/geometry_1024.bin", 1 + 2 + 16 + 128 + 1024);
	z.verify_geometry_file("H:/geometry_8192_0.bin", 2048);
	z.verify_geometry_file("H:/geometry_8192_1.bin", 2048);
	z.verify_geometry_file("H:/geometry_8192_2.bin", 2048);
	z.verify_geometry_file("H:/geometry_8192_3.bin", 2048);
	z.verify_geometry_file("H:/geometry_65536_0.bin", 16384);
	z.verify_geometry_file("H:/geometry_65536_1.bin", 16384);
	z.verify_geometry_file("H:/geometry_65536_2.bin", 16384);
	z.verify_geometry_file("H:/geometry_65536_3.bin", 16384);
	z.verify_geometry_file("H:/geometry_0_13113.bin", 13114 * 64);
	z.verify_geometry_file("H:/geometry_13114_26227.bin", 13114 * 64);
	z.verify_geometry_file("H:/geometry_26228_39341.bin", 13114 * 64);
	z.verify_geometry_file("H:/geometry_39342_52455.bin", 13114 * 64);
	z.verify_geometry_file("H:/geometry_52456_65535.bin", 13080 * 64);
	z.assemble_geometry();
	z.verify_geometry_file("H:/geometry.bin", 1 + 2 + 16 + 128 + 1024 + 8192 + 65536 + 65536 * 64);

	return 0;
}
*/