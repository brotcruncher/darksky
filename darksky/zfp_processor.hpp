/*
/ all zfp related operations are encapsulated in this class
*/

#pragma once
#include "zfp_file.hpp"
#include "../libraries/glm/glm.hpp"
#include <vector>
#include "block_file.hpp"


namespace darksky
{
	class zfp_processor
	{
		public:

		struct zfp_subblock
		{
			unsigned int first, length;
		};

		void attach_sources(std::string directory_path, std::string sizes_path, std::string working_directory, unsigned int number_draws); // attach sizes file (octreeStats_6.bin) and 
		
		void build_subdivision_info(std::string path); //  attach sdf octree file ds14_a_1.0000
		
		void find_velocity(unsigned int first_index, unsigned int last_index);
		void create_blockfile_from_lod(std::string input_path, std::string output_path, float scaling);

		void create_lod(unsigned int index);
		bool lods_equal(std::string first, std::string second);
		void level2_lod_summit(unsigned int level_1);
		void level1_lod_summit(unsigned int level_1);
		void level0_lod_summit();
		void verify_level1(unsigned int level_1);
		void verify_level2(unsigned int level_1);
		void check_level2_l2_availability();

		void extract_geometry_to_1024();
		void extract_geometry_8192(unsigned int level_1);
		void extract_geometry_65536(unsigned int level_1);
		void build_subdivision_geometry();
		void assemble_geometry();
		void verify_geometry_file(std::string path, unsigned int size);

		void assemble_particle_data_1024();
		void assemble_particle_data_8192(unsigned int level1);
		void assemble_particle_data_65536(unsigned int level1);
		void verify_particle_data(std::string path, uint64_t size);

		void block_file_assembly(std::string block_file_path);
		void verify_block_file(std::string block_file_path);

		private:

		void convert_particles(zfp_particle* zfp_buffer, block_file::particle* part_buffer, float scaling, float alpha_scaling);
		std::string construct_path(unsigned int index);
		std::vector<unsigned int> draw_n_from_m(unsigned int n, unsigned int m);
		

		glm::vec4 get_lod_geometry(std::string lod_path, zfp_particle* zfp_buffer);

		std::string directory_path;
		std::ifstream sizes_file;
		zfp_file file_buffer;
		zfp_particle* particle_buffer;
		int64_t* m_sizes_buffer;
		int64_t m_max_size;
		zfp_particle* m_particle_buffer;
		std::string m_working_directory; 
		unsigned int m_number_draws; 
	};
}