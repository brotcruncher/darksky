/*
/	This classes provide an wrapper on OpenGL function for shaders programms and shader storage buffer objects
*/

#pragma once
#include <string>
#include "GL\glew.h"

namespace darksky
{
	using namespace std;

	class shader_program
	{
		public:
		
		shader_program();
		void attach(GLenum type, string path);
		void link();
		void use();
	
		private:

		GLuint id;
	};
	
	class shader_storage
	{
		public:

		shader_storage();
		void write_from(GLuint size, GLvoid *data);
		void read_to(GLuint size, GLvoid *data);
		void bind(GLuint position);
		void allocate(GLuint size);
		GLvoid* map(GLenum access);
		void unmap();

		private:

		GLuint id;
		GLvoid* mapping;
	};
}