#include <iostream>
#include "block_file.hpp"

namespace darksky
{
	block_file::block_file()
	{
	}

	void block_file::attach_source(std::string path)
	{
		info.source.open(path, std::ios::binary | std::ios::beg);
		info.source.read((char*)&info.block_count, sizeof(unsigned int));
		info.source.read((char*)&info.zfp_count, sizeof(unsigned int));

		info.block_info = (block*)malloc(info.block_count * sizeof(block));
		info.source.read((char*)info.block_info, sizeof(block) * info.block_count);
		info.lod_particle_count = info.block_info[0].particle_count;

		info.zfp_subblocks = (zfp_subblock*)malloc(info.zfp_count * 64 * sizeof(zfp_subblock));
		info.source.read((char*)info.zfp_subblocks, info.zfp_count * 64 * sizeof(zfp_subblock));

		info.geometry_start = sizeof(unsigned int) + sizeof(unsigned int) + sizeof(block) * info.block_count + info.zfp_count * 64 * sizeof(zfp_subblock);
		info.particle_start = info.geometry_start + (info.block_count + info.zfp_count * 64) * sizeof(glm::vec4);

	//	glm::vec4 test;

	//	info.source.seekg(info.geometry_start + 74899 * sizeof(glm::vec4));

		// for(unsigned int i = 0; i < info.lod_particle_count; i++)
		//{
		//	info.source.read((char*)&test, sizeof(glm::vec4));
		//	std::cout << i <<  std::fixed << ") x:" << test.x << " y:" << test.y << " z:" << test.z << std::endl;
		//}
 	}

	void block_file::read_geometry(void* destination)
	{
		info.source.seekg(info.geometry_start);
		info.source.read((char*)destination, info.block_count * sizeof(glm::vec4));
	}

	void block_file::read_particles(unsigned int block, void* destination)
	{
		if (block >= info.block_count)
		{
			std::cout << "[ERROR] block_file::read_particles: cannot read zfp subblock from block file";
			return;
		}

		info.source.seekg(info.particle_start + block * info.lod_particle_count * sizeof(particle));
		info.source.read((char*)destination, info.lod_particle_count * sizeof(particle));
	}

	block_file::zfp_subblock* block_file::get_subblocks(unsigned int index)
	{
		return &info.zfp_subblocks[index * 64];
	}

	block_file::block* block_file::get_info(unsigned int block_index)
	{
		return &info.block_info[block_index];
	}

	unsigned int block_file::get_block_count()
	{
		return info.block_count;
	}

	unsigned int block_file::get_zfp_count()
	{
		return info.zfp_count;
	}
}