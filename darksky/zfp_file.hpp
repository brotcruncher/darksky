/*
/ encapsulates an zfp file for convenient reading and handling
*/

#pragma once
#include <string>
#include <fstream>

namespace darksky
{
	#pragma pack(push, 1)
	struct zfp_particle
	{
		float x, y, z, r, g, b;
	};
	#pragma pack(pop)


	#pragma pack(push, 1)
	struct zfp_header
	{
		float accuracy;
		uint64_t channel_sizes[6];
	};
	#pragma pack(pop)

	class zfp_file
	{
		public:
		zfp_file();
		void resize_capacity(unsigned int byte_count, unsigned int particle_count); // zfp needs decompression and this specifies the buffer size avaiable for it, I used byte_count = 100MB and particle count max_size
		void attach_source(std::string source_path); // attach an zfp file to class and read initial information
		void read_particles(unsigned int particle_count, zfp_particle* particle_data); // decompress zfp particles and read them to particle_data
		void attach_source(std::string darksky_directory, unsigned int index);

		private:
		std::ifstream source;
		zfp_header header;
		char* read_buffer;
		float* raw_buffer;
	};
}