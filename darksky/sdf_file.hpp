#pragma once
#include <string>
#include <fstream>
#include "../libraries/glm/glm.hpp"
#include <map>

namespace darksky
{
	using namespace std;

	struct sdf_header
	{
		int header_len;
		long long nhalo;
		double BOX_SIZE;
		double L0;
		double R0;
		int offset_center;
		double SCALE_NOW;
		double a;
		int do_periodic;
		double overdensity;
		double redshift;
		double tpos;
		float part_mass;
		float particle_mass;
		double Omega0_m;
		double Omega0_lambda;
		double h_100;
		int so200b;
		int rockstar_units;
		int morton_xyz;
		float x_min;
		float y_min;
		float z_min;
		float x_max;
		float y_max;
		float z_max;
		int sha1_chunks;
		string length_unit;
		string mass_unit;
		string time_unit;
		string velocity_unit;
		string compiled_version_2HOT;
		string compiled_date_2HOT;
		string compiled_time_2HOT;
	};
	
	#pragma pack(push, 1)
	struct sdf_point
	{
		glm::vec3 position;
		glm::vec3 velocity;
		float mvir, m200b, m200c, m500c, m2500c;
		float vmax, spin, kin_to_pot;
		long long id, pid;
	};
	#pragma pack(pop)

	class sdf_file
	{
		public:
			sdf_file() = delete;
			sdf_file(string path);
			void read_points(long start, long count, sdf_point* buffer);
			sdf_header header;

		private:
			ifstream file;
			long long start;
			long long eof;
	};
}