/*
/	This class offers a RAM lru cache with variable size 
*/

#pragma once
#include "block_file.hpp"

namespace darksky
{
	class lru_cache
	{
		public:

			void initialize_cache(block_file& source, unsigned int slots); // setup information to maintain cache
			typedef block_file::particle* slot_data_t; 
			slot_data_t& request_block(unsigned int block); // query if block is cached or not and load it

		private:

			void print_state();

			block_file* source;

			unsigned int slots;

			slot_data_t* slot_data;

			struct slot_info_t
			{
				unsigned int previous;
				unsigned int resident;
				unsigned int next;
			};

			slot_info_t* slot_info; // basicly a linked list that is updated with every requested block
			unsigned int mru_slot; // most recently used slot
			unsigned int lru_slot; // least recently used slot

			struct block_info_t
			{
				unsigned int slot;
			};

			block_info_t* block_info; // mapping block->slot

			unsigned int free_slots;

			struct
			{
				unsigned int block_count;
				unsigned int zfp_count;
				unsigned int total_count;
				unsigned int lod_particle_count;
			} block_file_info;
	};
}