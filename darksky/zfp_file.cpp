#include "zfp_file.hpp"
#include <iostream>
#include "../libraries/zfp/inc/zfp.h"
#include <chrono>

namespace darksky
{
	zfp_file::zfp_file()
	{
		read_buffer = nullptr;
		raw_buffer = nullptr;
	}

	void zfp_file::attach_source(std::string source_path)
	{
		if (this->source.is_open()) this->source.close();

		this->source.open(source_path, std::ios::beg | std::ios::binary);
		if(!this->source.is_open())
		{
			std::cout << "[error] [zfp_file] source could not be opened." << std::endl;
			return;
		}

		this->source.read((char*)&header, sizeof(zfp_header));
	}

	void zfp_file::attach_source(std::string darksky_directory, unsigned int index)
	{
		unsigned int level1 = index / 16384;
		unsigned int level2 = level1 * 128 + (index - level1 * 16384) / 128;

		std::string index_prefix = "000";
		if (index < 10000)
		{
			if (index < 1000)
			{
				if (index < 100)
				{
					if (index < 10) index_prefix = "0000000";
					else index_prefix = "000000";
				}
				else index_prefix = "00000";
			}
			else index_prefix = "0000";
		}

		std::string level1_prefix = "000000";
		if (level1 < 10) level1_prefix = "0000000";

		std::string level2_prefix = "0000";
		if (level2 < 1000)
		{
			if (level2 < 100)
			{
				if (level2 < 10) level2_prefix = "0000000";
				else level2_prefix = "000000";
			}
			else level2_prefix = "00000";
		}

		if (this->source.is_open()) this->source.close();

		this->source.open(darksky_directory + level1_prefix + std::to_string(level1) + "/" + level2_prefix + std::to_string(level2) + "/" + index_prefix + std::to_string(index) + ".zfp", std::ios::beg | std::ios::binary);
		
		if (!this->source.is_open())
		{
			std::cout << "[error] [zfp_file] source could not be opened." << std::endl;
			return;
		}

		this->source.read((char*)&header, sizeof(zfp_header));
	}


	void zfp_file::resize_capacity(unsigned int byte_count, unsigned int particle_count)
	{	
		if (read_buffer != nullptr) free(read_buffer);
		if (raw_buffer != nullptr) free(read_buffer);

		read_buffer = (char*)calloc(byte_count, sizeof(char));
		raw_buffer = (float*)calloc(particle_count * 6, sizeof(float));
	}

	void zfp_file::read_particles(unsigned int particle_count, zfp_particle* particle_data)
	{
		uint64_t size = 0;
		for (unsigned int i = 0; i < 6; i++)
		{
			size += header.channel_sizes[i];
		}
	
		this->source.read(read_buffer, size);

		size_t location = 0;
		for (unsigned int i = 0; i < 6; i++)
		{
			zfp_field* f = zfp_field_1d((void*)&raw_buffer[i * particle_count], zfp_type_float, particle_count);
			zfp_field_set_stride_1d(f, 1);

			bitstream* s = stream_open(&read_buffer[location], size * sizeof(float));
			zfp_stream* z = zfp_stream_open(s);
			zfp_stream_set_accuracy(z, header.accuracy, zfp_type_float);
			
			int result = zfp_decompress(z, f);

			zfp_stream_close(z);
			stream_close(s);
			zfp_field_free(f);

			if (!result)
			{
				std::cout << "[error] [zfp_file] decompression failed." << std::endl;
				return;
			}

			location += header.channel_sizes[i];
		}

		for(unsigned int i = 0; i < particle_count; i++)
		{
			zfp_particle& current_particle = particle_data[i];
			current_particle.x = raw_buffer[i];
			current_particle.y = raw_buffer[i + particle_count];
			current_particle.z = raw_buffer[i + particle_count * 2];
			current_particle.r = raw_buffer[i + particle_count * 3];
			current_particle.g = raw_buffer[i + particle_count * 4];
			current_particle.b = raw_buffer[i + particle_count * 5];
		}
	}
}