#include "particle_renderer.hpp"
#include <iostream>
#include "../libraries/glm/gtc/matrix_transform.hpp"
#include "../libraries/glm/gtx/rotate_vector.hpp"
#include "../libraries/glm/gtc/reciprocal.hpp"
#include <vector>

namespace darksky
{
	// callbacks for error handling of OpenGL and glfw

	void glfw_error_callback(int error, const char* description)
	{
		std::cout << "[particle_renderer] glfw error: " << description << std::endl;
	}

	void framebuffer_size_callback(GLFWwindow* window, int width, int height)
	{
		glViewport(0, 0, width, height);
	}

	void APIENTRY openglCallbackFunction(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, GLvoid* userParam)
	{
		if(severity == GL_DEBUG_SEVERITY_MEDIUM || severity == GL_DEBUG_SEVERITY_HIGH) std::cout << message << std::endl;
	}

	/*
	/	Constructor handles OpenGL context creation, ssbo and shader initialization and camera setup
	*/

	particle_renderer::particle_renderer()
	{
		glfwSetErrorCallback(glfw_error_callback);

		// create OpenGL 4.0 Debug Context

		if (!glfwInit()) exit(EXIT_FAILURE);

		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
		window = glfwCreateWindow(1024, 720, "Darksky Visualization", NULL, NULL);

		if (!window)
		{
			glfwTerminate();
			exit(EXIT_FAILURE);
		}

		glfwMakeContextCurrent(window);
		glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

		glewExperimental = GL_TRUE;
		if (glewInit() != GLEW_OK)
		{
			std::cout << "[particle_renderer] glew initialization error." << std::endl;
			exit(EXIT_FAILURE);
		}

		std::cout << "[particle_renderer] rendering context set up." << std::endl;

		glDebugMessageCallback(openglCallbackFunction, nullptr);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, true);

		// setup ssbos and shader programs

		additive_shader = new shader_program;
		framebuffer_shader = new shader_program;
		compute_shader = new shader_program;

		matrices_buffer = new shader_storage;
		geometry_buffer = new shader_storage;
		result_buffer = new shader_storage;
		count_buffer = new shader_storage;
		frustum_buffer = new shader_storage;
		resolution_buffer = new shader_storage;

		// initialize camera to look at dataset 

		camera.position = glm::vec3(5000000, 5000000, 5000000);
		camera.direction = glm::vec3(-4590686, -5399741, -5469571);
		camera.view = glm::lookAt(camera.position, camera.direction, glm::vec3(0, 1, 0));
		camera.projection = glm::perspective(glm::radians(60.f), 1024.f / 720.f, 0.1f, 100000000.f);
		camera.resolution = glm::ivec2(1024, 720);

		// compile and link shader programs

		additive_shader->attach(GL_VERTEX_SHADER, "../darksky/render_additive.glvs");
		additive_shader->attach(GL_FRAGMENT_SHADER, "../darksky/render_additive.glfs");
		additive_shader->link();

		framebuffer_shader->attach(GL_VERTEX_SHADER, "../darksky/render_framebuffer.glvs");
		framebuffer_shader->attach(GL_FRAGMENT_SHADER, "../darksky/render_framebuffer.glfs");
		framebuffer_shader->link();

		compute_shader->attach(GL_COMPUTE_SHADER, "../darksky/project_hierarchy.glcs");
		compute_shader->link();

		glGenVertexArrays(1, &vao);
		glGenFramebuffers(1, &framebuffer);
		glGenTextures(1, &texture);
	}

	/*
	/	Initializes Renderer, including:
	/	- setting up SSBOs
	/	- setting up VRAM lru-cache
	/	- extracting geometry information from block file for projection
	*/

	void particle_renderer::initialize_renderer(unsigned int slots, block_file& source, lru_cache& cache)
	{
		this->source = &source;
		this->cache = &cache;
		this->slots = slots;

		slot_buffer = (shader_storage**)calloc(slots, sizeof(shader_storage*)); 

		// find out ssbo slot size: root node 0 always has max size
		unsigned int block_size = source.get_info(0)->particle_count; 

		// initialize ssbos with preallocated memeory
		for (unsigned int i = 0; i < slots; i++) 
		{
			shader_storage*& current_storage = slot_buffer[i];
			current_storage = new shader_storage;
			current_storage->allocate(block_size * sizeof(block_file::particle)); 
		}

		// allocate information to maintain the VRAM lru cache
		slot_info = (slot_info_t*)malloc(slots * sizeof(slot_info_t));
		for (unsigned int i = 0; i < slots; i++)
		{
			slot_info[i].previous = -1u;
			slot_info[i].resident = -1u;
			slot_info[i].next = -1u;
		}

		mru_slot = -1u;
		lru_slot = -1u;

		unsigned int block_count = source.get_block_count();
		unsigned int zfp_count = source.get_zfp_count();
		unsigned int combined_size = block_count + 64 * zfp_count;

		// allocate block_count * block_info
		block_info = (block_info_t*)malloc(combined_size * sizeof(block_info_t));
		for (unsigned int i = 0; i < combined_size; i++)
		{
			block_info[i].slot = -1u;
		}

		// allocate free list for slots 
		free_slots = slots;

		geometry_buffer->allocate(sizeof(glm::vec4) * combined_size); // about 70MB
		GLvoid* geometry_buffer_mapping = geometry_buffer->map(GL_WRITE_ONLY);
		source.read_geometry(geometry_buffer_mapping);
		geometry_buffer->unmap();

		result_buffer->allocate(combined_size * sizeof(glm::uint));
		count_buffer->write_from(sizeof(glm::uint), (glm::uint*)&combined_size);

		draw_list = (unsigned int*)malloc(sizeof(unsigned int) * combined_size);
	}
	
	/*
	/	upload_block takes a block index as argument and uploads this block into the VRAM.
	/   If this block is already cached it simply registers it as mru and returns.
	/	If this block is not cached it sends an request to the RAM lru_cache and uploads it on response
	*/

	void particle_renderer::upload_block(unsigned int block)
	{
		if (block_info[block].slot != -1u) // already cached
		{
			slot_info_t& current_slot = slot_info[block_info[block].slot];

			if (current_slot.next != -1u) // if block != mru
			{
				if (current_slot.previous != -1u) // if block != lru
				{
					slot_info[current_slot.previous].next = current_slot.next;
					slot_info[current_slot.next].previous = current_slot.previous;
				}
				else // if block == lru
				{
					lru_slot = current_slot.next;
					slot_info[lru_slot].previous = -1u;
				}

				current_slot.next = -1u;
				current_slot.previous = mru_slot;

				slot_info[mru_slot].next = block_info[block].slot;

				mru_slot = block_info[block].slot;
			}

			return;
		}
		else
		{
			lru_cache::slot_data_t& cache_slot = cache->request_block(block);

			// get slot to upload block to

			unsigned int slot;

			if (free_slots == 0) // if no free slots: use lru slot
			{
				slot = lru_slot;
				slot_info_t& current_slot = slot_info[lru_slot];
				block_info[current_slot.resident].slot = -1u;
				lru_slot = current_slot.next;
			}
			else // else: use free slot
			{
				slot = slots - free_slots;
				free_slots -= 1;
			}

			slot_info_t& current_slot = slot_info[slot];

			current_slot.resident = block;
			block_info[block].slot = slot;

			if (lru_slot == -1u) // if slot is first used slot: initialize lru_slot
			{
				current_slot.previous = -1u;
				lru_slot = slot;
			}
			else // else: update last mru_slot
			{
				slot_info[mru_slot].next = slot;
				current_slot.previous = mru_slot;
			}

			current_slot.next = -1u;
			mru_slot = slot;

			unsigned int block_size = source->get_info(0)->particle_count; // verzweigen f�r zfp loading

			shader_storage* slot_storage = slot_buffer[slot];
			GLvoid* destination = slot_storage->map(GL_WRITE_ONLY);
			memcpy(destination, cache_slot, block_size * sizeof(block_file::particle));
			slot_storage->unmap();
		}
	}
	
	bool init = true;

	/*
	/	handles input and updates camera accordingly
	*/

	void particle_renderer::update_camera()
	{
		bool camera_update = false;
		bool resolution_update = false;

		std::chrono::high_resolution_clock::time_point current_frame = std::chrono::high_resolution_clock::now();

		if (init) // on first run update everything to initialize data before rendering
		{
			camera_update = true;
			resolution_update = true;
			init = false;
			last_frame = current_frame;
		}

		float frametime = std::chrono::duration_cast<std::chrono::milliseconds>(current_frame - last_frame).count() / 10.f;
		last_frame = current_frame;

		glfwPollEvents();
		
		if (glfwWindowShouldClose(window) == true)
		{
			glfwDestroyWindow(window);
			exit(EXIT_SUCCESS);
		}

		int width, height;
		glfwGetWindowSize(window, &width, &height);

		if(camera.resolution.x != width || camera.resolution.y != height)
		{
			resolution_update = true;
			camera.resolution.x = width;
			camera.resolution.y = height;
		}
		 
		// arrow key controls: UP = forward, DOWN = backward, LEFT = left, RIGHT = right

		if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
		{
			camera.position += 0.005f * frametime * camera.direction;
			camera_update = true;
		}

		if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
		{
			camera.position -= 0.005f * frametime * camera.direction;
			camera_update = true;
		}

		if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
		{
			camera.position -= 0.005f * frametime * glm::cross(camera.direction, glm::vec3(0, 1, 0));
			camera_update = true;
		}

		if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		{
			camera.position += 0.005f * frametime * glm::cross(camera.direction, glm::vec3(0, 1, 0));
			camera_update = true;
		}

		// on click rotate the camera direction drag and drop style

		if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
		{
			glm::dvec2 new_position;
			glfwGetCursorPos(window, &new_position.x, &new_position.y);
			camera.direction = glm::rotateX(camera.direction, 0.0005f * float(mouse.y - new_position.y));
			camera.direction = glm::rotateY(camera.direction, 0.0005f * float(new_position.x- mouse.x));
			camera_update = true;
		}

		glfwGetCursorPos(window, &mouse.x, &mouse.y);

		// if camera has beed updated (moved or rotated): frustum update

		if (camera_update)
		{
			camera.view = glm::lookAt(camera.position, camera.position + camera.direction, glm::vec3(0, 1, 0));

			glm::mat4 view_projection = camera.projection * camera.view;
			camera.frustum[0] = glm::vec4(view_projection[0][3] - view_projection[0][0], view_projection[1][3] - view_projection[1][0], view_projection[2][3] - view_projection[2][0], view_projection[3][3] - view_projection[3][0]); // Right
			camera.frustum[1] = glm::vec4(view_projection[0][3] + view_projection[0][0], view_projection[1][3] + view_projection[1][0], view_projection[2][3] + view_projection[2][0], view_projection[3][3] + view_projection[3][0]); // Left
			camera.frustum[2] = glm::vec4(view_projection[0][3] + view_projection[0][1], view_projection[1][3] + view_projection[1][1], view_projection[2][3] + view_projection[2][1], view_projection[3][3] + view_projection[3][1]); // Bottom
			camera.frustum[3] = glm::vec4(view_projection[0][3] - view_projection[0][1], view_projection[1][3] - view_projection[1][1], view_projection[2][3] - view_projection[2][1], view_projection[3][3] - view_projection[3][1]); // Top
			camera.frustum[4] = glm::vec4(view_projection[0][3] - view_projection[0][2], view_projection[1][3] - view_projection[1][2], view_projection[2][3] - view_projection[2][2], view_projection[3][3] - view_projection[3][2]); // Far
			camera.frustum[5] = glm::vec4(view_projection[0][3] + view_projection[0][2], view_projection[1][3] + view_projection[1][2], view_projection[2][3] + view_projection[2][2], view_projection[3][3] + view_projection[3][2]); // Near

			for (unsigned int i = 0; i < 6; i++)
			{
				float length = glm::length(glm::vec3(camera.frustum[i].x, camera.frustum[i].y, camera.frustum[i].z));
				camera.frustum[i] /= length;
			}

			frustum_buffer->write_from(sizeof(glm::vec4) * 6, &camera.frustum);
		}

		// if resolution has been updated (window resize): update framebuffer and texture, projection matrix and resolution buffer

		if (resolution_update)
		{
			std::cout << "new resolution:" << camera.resolution.x << " x " << camera.resolution.y << std::endl;

			camera.projection = glm::perspective(glm::radians(60.f), static_cast<float>(camera.resolution.x) / camera.resolution.y, 0.1f, 100000000.f);

			glBindTexture(GL_TEXTURE_2D, texture);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, camera.resolution.x, camera.resolution.y, 0, GL_RGBA, GL_FLOAT, 0);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);

			if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) std::cout << "framebuffer: error" << std::endl;

			glBindFramebuffer(GL_FRAMEBUFFER, 0);

			resolution_buffer->write_from(sizeof(glm::ivec2), &camera.resolution);
		}

		if(camera_update || resolution_update)
		{
			matrices_buffer->write_from(sizeof(glm::mat4) * 2, &camera.view);
		}
	}

	/*
	/	clear screen and texture
	*/

	void particle_renderer::clear_buffers()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
		glClearColor(0, 0, 0, 0);
		glClear(GL_COLOR_BUFFER_BIT);
	
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glClearColor(0, 0, 0, 0);
		glClear(GL_COLOR_BUFFER_BIT);
	}

	void particle_renderer::swap_buffers()
	{
		glfwSwapBuffers(window);
	}

	/*
	/	draw a single block
	*/

	void particle_renderer::draw_block(unsigned int block)
	{
		if (block_info[block].slot == -1u) upload_block(block); // better: error and start task

		glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

		glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		glEnable(GL_BLEND);
		
		glBindVertexArray(vao);
		additive_shader->use();
		matrices_buffer->bind(0);
		resolution_buffer->bind(1);

		unsigned int slot = block_info[block].slot;
		unsigned int size;
		if(block >= source->get_block_count())
		{
			unsigned int zfp_index = (block - source->get_block_count()) / 64;
			size = glm::min(source->get_subblocks(zfp_index)[block - source->get_block_count() - zfp_index * 64].length, 100000u);
			std::cout << "draw subblock " << block << " from slot " << slot << " (info: " << size << ")" << std::endl;
		}
		else size = source->get_info(block)->particle_count;

		shader_storage* slot_storage = slot_buffer[slot];
		slot_storage->bind(2);
		glDrawArrays(GL_POINTS, 0, size);

		glDisable(GL_BLEND);                                                           
		glDisable(GL_VERTEX_PROGRAM_POINT_SIZE);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	/*
	/	draw offscreen texture with tonemapping to screen
	*/

	void particle_renderer::draw_framebuffer()
	{
		glBlendFunc(GL_ONE, GL_ONE);

		glBindVertexArray(vao);
		framebuffer_shader->use();
		glBindTexture(GL_TEXTURE_2D, texture);

		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	/*
	/	project geometry with compute shader and evaluate result on CPU side to determine which blocks to draw and load
	*/

	void particle_renderer::update_blocks()
	{
		compute_shader->use();
		matrices_buffer->bind(0);
		resolution_buffer->bind(1);
		frustum_buffer->bind(2);
		geometry_buffer->bind(3);
		result_buffer->bind(4);
		count_buffer->bind(5);

		glDispatchCompute(32, 16, 16);

		glm::uint* result_buffer_mapping = (glm::uint*)result_buffer->map(GL_READ_WRITE);
		if (result_buffer_mapping[0] == 0) result_buffer_mapping[0] = 1;
		
		// doubleganger check

		unsigned int combined_size = source->get_block_count() + 64 * source->get_zfp_count();

		std::vector<unsigned int> check;
		check.reserve(combined_size);
		check.push_back(0);

		unsigned int last_checked = 0;

		while(check.size() > 0)
		{
			glm::uint& current_result = result_buffer_mapping[check[0]];

			if (current_result == 1) // if large enough to be drawn ...
			{
				last_checked = check[0];

				block_file::block& block = *source->get_info(check[0]);

				bool draw_child = false;

				for (unsigned int i = 0; i < block.child_count; i++) // ... check if children are too. ..
				{
					if (result_buffer_mapping[block.first_child + i]) draw_child = true;
					check.push_back(block.first_child + i);
				}

				if (draw_child) // ... if so, dont draw parent node
				{
					current_result = 0;

					for (unsigned int i = 0; i < block.child_count; i++)
					{
						result_buffer_mapping[block.first_child + i] = 1; 
					}
				}
			}
			
			check[0] = check.back();
			check.pop_back();
		}

		draw_count = 0; // reset draw list

		// register all blocks to be drawm

		for (unsigned int i = 0; i < last_checked + 1; i++) 
		{
			// std::cout << i << ") = " << result_buffer_mapping[i] << std::endl;
			if (result_buffer_mapping[i])
			{
				draw_list[draw_count] = i; // enlist to draw list
				draw_count++;// push draw_count
			}
		}
	}

	/*
	/	draw all blocks in draw list
	*/

	void particle_renderer::draw_blocks()
	{
		for(unsigned int drawable = 0; drawable < draw_count; drawable++)
		{
			draw_block(draw_list[drawable]);
		}
	}
}