#pragma once
#include <string>
#include <fstream>
#include "../libraries/glm/glm.hpp"

namespace darksky
{
	/*
	The block file holds the data of the precomputed subdivision hierarchy of the original simulation data.
	This class provies an interface that frees the programmer from manually reading the block file and retrieving the particle data.
	The leaf blocks are stored in seperate .zfp files of about 120 Megabyte size. These files are specified by their index.
	The particles of the non-leaf blocks are stored similar to the standalone block file structure.

	structure of block file:
	{
		unsigned int block_count;
		unsigned int zfp_count;

		struct
		{
			unsigned int child_count;
			unsigned int first_child;
			unsigned int particle_count; // redundante info
			unsigned int first_particle;
		} block_info[block_count];

		struct
		{
			unsigned int first;
			unsigned int length;
		} subdivision_data[64 * zfp_count];

		glm::vec4 block_geometry[block_count + 64 * zfp_count];

		struct
		{
			glm::vec4 geometry;
			glm::uint color;
		} particle_data[];
	}
	*/

	class block_file
	{
		public:

		block_file();
		void attach_source(std::string path);
		void read_particles(unsigned int block, void* destination); 
		void read_geometry(void* destination);
		unsigned int get_block_count();	
		unsigned int get_zfp_count();

		#pragma pack(push, 1)
		struct block
		{
			unsigned int child_count;
			unsigned int first_child;
			unsigned int particle_count;
			unsigned int first_particle;
		};
		#pragma pack(pop)

		block* get_info(unsigned int block_index);
			
		#pragma pack(push, 1)
		struct particle
		{
			glm::vec4 geometry;
			glm::uint color;
		};
		#pragma pack(pop)

		#pragma pack(push, 1)
		struct zfp_subblock
		{
			unsigned int first, length;
		};
		#pragma pack(pop)

		zfp_subblock* get_subblocks(unsigned int index);

		private:

		struct
		{
			std::ifstream source;
			bool type;
			unsigned int block_count;
			unsigned int zfp_count;
			unsigned int geometry_start;
			unsigned int particle_start;
			unsigned int lod_particle_count;
			block* block_info;
			zfp_subblock* zfp_subblocks;
		} info;
	};
}