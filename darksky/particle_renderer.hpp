/*
/	This file holds the particle_renderer class, which is used to handle all GPU related actions like OpenGL context creation, particle drawing, tonemapping,
/	geometry projection and VRAM caching but also handles input and window creation.
*/

#pragma once
#include "block_file.hpp"
#include "GL\glew.h"
#include "GLFW\glfw3.h"
#include "shader_toolkit.hpp"
#include "../libraries/glm/glm.hpp"
#include <chrono>
#include "zfp_file.hpp"
#include "lru_cache.hpp"

namespace darksky
{
	class particle_renderer
	{
		public:

			particle_renderer(); // creates OpenGL context and windows and creates initial datastructures
			void initialize_renderer(unsigned int slots, block_file& source, lru_cache& cache); // sets up VRAM lru cache and registers source file and RAM lru cache
			void update_camera(); // reads input and resolution updates to update camera accordingly
			void update_blocks(); // dispatches compute to project hierarchy to 
			void draw_framebuffer(); // draws the offscreen texture with tonemapping applied to screen
			void clear_buffers(); // clears framebuffer and texture
			void swap_buffers(); 
			void draw_block(unsigned int block); // draws a single block
			void draw_blocks(); // draws all blocks in the draw_list

		private:

			void upload_block(unsigned int block);

			block_file* source;
			lru_cache* cache;

			GLFWwindow* window;
			glm::dvec2 mouse;

			GLuint vao; // empty vao because OpenGL loves them
			GLuint framebuffer; // the framebuffer object used to alternate between 
			GLuint texture; // the offscreen thesture

			shader_program* additive_shader; // draws particles with additive rendering to framebuffer
			shader_program* framebuffer_shader; // draws framebuffer with ACES tonemapping to screen
			shader_program* compute_shader; // evaluates geometry of the hierarchy to decide which blocks to render

			shader_storage* matrices_buffer; // holds view and projection matrix
			shader_storage* geometry_buffer; // holds vec4 with centroid and diameter for each block in the hierarchy 
			shader_storage* result_buffer; // the wayback from compute shader to CPU for continuing evaluation  
			shader_storage* count_buffer; // holds number of blocks in geometry_buffer; necessary because compute shader invocations are controlled at coarse level 
			shader_storage* frustum_buffer; // holds view frustum for frustum culling
			shader_storage* resolution_buffer; // holds current window resolution for particle projection and point_size calculation

			#pragma pack(push, 1)
			struct camera_t
			{
				glm::vec3 position;
				glm::vec3 direction;
				glm::mat4 view;
				glm::mat4 projection;
				glm::vec4 frustum[6];
				glm::ivec2 resolution;
			} camera;
			#pragma pack(pop) 

			// data necessary to maintain the VRAM lru cache, including ...
			unsigned int slots;
			shader_storage** slot_buffer;

			// ... a list with slots pointing to each other ...
			struct slot_info_t
			{
				unsigned int previous;
				unsigned int resident;
				unsigned int next;
			};

			slot_info_t* slot_info;
			unsigned int mru_slot; // most recently used slot
			unsigned int lru_slot; // least recently used slot
			unsigned int free_slots;

			// ... for each block of the hierarchy the slot it is saved in
			struct block_info_t 
			{
				unsigned int slot;
				// particle_count info for zfp blocks
			};

			block_info_t* block_info;

			// timepoints are used to determine the time elapsed between frames to regulate the target_size

			std::chrono::high_resolution_clock::time_point last_frame;

			// the draw list is resetted per frame and 
			unsigned int draw_count;
			unsigned int* draw_list;
	};
}