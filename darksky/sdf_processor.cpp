#include "sdf_processor.hpp"
#include <iostream>
#include <string>
#include <algorithm> 
#include <random>
#include <set>

namespace darksky
{
	sdf_preprocessor::sdf_preprocessor()
	{
	}

	void sdf_preprocessor::attach_source(std::string source_file)
	{
		this->source_file = new sdf_file(source_file);
	}

	#define buffer_size 10000000u

	void sdf_preprocessor::create_blocks(std::string path, unsigned int target_size)
	{
		#pragma pack(push, 1)
		struct binary_node
		{
			glm::vec3 min_position;
			glm::vec3 max_position;
			unsigned int size; // how many particles inside node
			unsigned int level; // level in hierarchy starting with 0
			unsigned int first_point; // file position of first point in node
			unsigned int last_point; // file position of last point in node
			unsigned int first_child;
			unsigned int child_count;

		};
		#pragma pack(pop)

		std::vector<binary_node> tree_nodes;
		const unsigned int number_bins = 10000;
		unsigned int* axis_bins = (unsigned int*)calloc(number_bins, sizeof(unsigned int));
		sdf_point* sdf_buffer = (sdf_point*)calloc(buffer_size, sizeof(sdf_point));

		{
			binary_node root = { glm::vec3(0), glm::vec3(source_file->header.BOX_SIZE + 0.001f), source_file->header.nhalo, 0, 0, source_file->header.nhalo };
			tree_nodes.push_back(root);
		}

		for (unsigned int i = 0; i < tree_nodes.size(); )
		{
			binary_node& current_node = tree_nodes[i];

			std::cout << "current_node: " << i << "/" << tree_nodes.size();
			std::cout << " size: " << current_node.size;
			std::cout << " min: (" << current_node.min_position.x << ", " << current_node.min_position.y << ", " << current_node.min_position.z << ")";
			std::cout << " max: (" << current_node.max_position.x << ", " << current_node.max_position.y << ", " << current_node.max_position.z << ")";
			std::cout << " first:" << current_node.first_point << " last:" << current_node.last_point;

			if (current_node.size <= target_size)
			{
				i++;
				std::cout << " -> no split (current_node.size:" << current_node.size << " < " << target_size << ")" << std::endl << std::endl;
				continue;
			}

			unsigned int level = current_node.level % 3;

			unsigned int overall_read = 0;
			unsigned int current_read = 0;
			unsigned int target_read = current_node.last_point - current_node.first_point + 1;

			float step;

			switch (level)
			{
				case 0: step = (current_node.max_position.x - current_node.min_position.x) / number_bins; break;
				case 1: step = (current_node.max_position.y - current_node.min_position.y) / number_bins; break;
				case 2: step = (current_node.max_position.z - current_node.min_position.z) / number_bins; break;
			}

			while (overall_read != target_read)
			{
				current_read = glm::min(buffer_size, target_read - overall_read);
				source_file->read_points(current_node.first_point + overall_read, current_read, sdf_buffer);

				glm::vec3 position;
				unsigned int bin;

				//std::cout << overall_read << " / " << source_file->header.nhalo << std::endl;

				for (unsigned int p = 0; p < current_read; p++)
				{
					position = sdf_buffer[p].position;

					if (position.x >= current_node.min_position.x && position.x < current_node.max_position.x &&
						position.y >= current_node.min_position.y && position.y < current_node.max_position.y &&
						position.z >= current_node.min_position.z && position.z < current_node.max_position.z)
					{
						switch (level)
						{
							case 0: bin = (position.x - current_node.min_position.x) / step; break;
							case 1: bin = (position.y - current_node.min_position.y) / step; break;
							case 2: bin = (position.z - current_node.min_position.z) / step; break;
						}

						axis_bins[glm::min(bin, number_bins - 1u)]++;
					}
				}

				overall_read += current_read;
			}

			// get central bin

			unsigned int central_bin = 0;
			unsigned int before_central = 0;

			for (unsigned int p = axis_bins[0]; p < (unsigned int)(current_node.size / 2.f);)
			{
				central_bin++;
				before_central = p;
				p += axis_bins[central_bin];
			}

			// calc central bin range

			float current_node_min;

			switch (level)
			{
				case 0: current_node_min = current_node.min_position.x; break;
				case 1: current_node_min = current_node.min_position.y; break;
				case 2: current_node_min = current_node.min_position.z; break;
			}

			float central_min = current_node_min + central_bin * step;
			float central_max = current_node_min + (central_bin + 1) * step;

			std::vector<float> values_central_bin;
			values_central_bin.reserve(axis_bins[central_bin]);

			overall_read = 0;

			while (overall_read != target_read)
			{
				current_read = glm::min(buffer_size, target_read - overall_read);
				source_file->read_points(current_node.first_point + overall_read, current_read, sdf_buffer);

				glm::vec3 position;
				for (unsigned int p = 0; p < current_read; p++)
				{
					position = sdf_buffer[p].position;

					if (position.x >= current_node.min_position.x && position.x < current_node.max_position.x &&
						position.y >= current_node.min_position.y && position.y < current_node.max_position.y &&
						position.z >= current_node.min_position.z && position.z < current_node.max_position.z)
					{
						switch (level)
						{
							case 0: if (position.x >= central_min && position.x <= central_max) values_central_bin.push_back(position.x); break;
							case 1: if (position.y >= central_min && position.y <= central_max) values_central_bin.push_back(position.y); break;
							case 2: if (position.z >= central_min && position.z <= central_max) values_central_bin.push_back(position.z); break;
						}
					}
				}

				overall_read += current_read;
			}

			std::sort(values_central_bin.begin(), values_central_bin.end());

			unsigned int central_element = (unsigned int)(current_node.size / 2.f) - before_central - 1;

			float split = values_central_bin[central_element];

			std::cout << " -> split " << level << " at: " << split;

			binary_node first, second;

			switch (level)
			{
				case 0:
				first.max_position = glm::vec3(split, current_node.max_position.y, current_node.max_position.z);
				second.min_position = glm::vec3(split, current_node.min_position.y, current_node.min_position.z);
				break;

				case 1:
				first.max_position = glm::vec3(current_node.max_position.x, split, current_node.max_position.z);
				second.min_position = glm::vec3(current_node.min_position.x, split, current_node.min_position.z);
				break;

				case 2:
				first.max_position = glm::vec3(current_node.max_position.x, current_node.max_position.y, split);
				second.min_position = glm::vec3(current_node.min_position.x, current_node.min_position.y, split);
				break;
			}

			first.level = current_node.level + 1;
			second.level = current_node.level + 1;
			first.min_position = current_node.min_position;
			second.max_position = current_node.max_position;
			first.size = 0;
			second.size = 0;
			
			first.first_point = -1;
			second.first_point = -1;
			first.last_point = 0;
			second.last_point = 0;

			//first.last_point = current_node.last_point;
			//second.last_point = current_node.last_point;


			overall_read = 0;

			while (overall_read != target_read)
			{
				current_read = glm::min(buffer_size, target_read - overall_read);
				source_file->read_points(current_node.first_point + overall_read, current_read, sdf_buffer);

				glm::vec3 position;
				unsigned int file_position;

				for (unsigned int p = 0; p < current_read; p++)
				{
					position = sdf_buffer[p].position;
					file_position = p + overall_read + current_node.first_point;

					if (position.x >= current_node.min_position.x && position.x < current_node.max_position.x &&
						position.y >= current_node.min_position.y && position.y < current_node.max_position.y &&
						position.z >= current_node.min_position.z && position.z < current_node.max_position.z)
					{
						switch (level)
						{
							case 0:
							{
								if (position.x < first.max_position.x)
								{
									first.size++;
									if (first.first_point > file_position) first.first_point = file_position;
									else if (first.last_point < file_position) first.last_point = file_position;
								}
								else
								{
									second.size++;
									if (second.first_point > file_position) second.first_point = file_position;
									else if (second.last_point < file_position) second.last_point = file_position;
								}
								break;
							}
							case 1:
							{
								if (position.y < first.max_position.y)
								{
									first.size++;
									if (first.first_point > file_position) first.first_point = file_position;
									else if (first.last_point < file_position) first.last_point = file_position;
								}
								else
								{
									second.size++;
									if (second.first_point > file_position) second.first_point = file_position;
									else if (second.last_point < file_position) second.last_point = file_position;
								}
								break;
							}
							case 2:
							{
								if (position.z < first.max_position.z)
								{
									first.size++;
									if (first.first_point > file_position) first.first_point = file_position;
									else if (first.last_point < file_position) first.last_point = file_position;
								}
								else
								{
									second.size++;
									if (second.first_point > file_position) second.first_point = file_position;
									else if (second.last_point < file_position) second.last_point = file_position;
								}
								break;
							}
						}
					}
				}

				overall_read += current_read;
			}

			// insert and swap delete current node

			if (level == 0)
			{
				i++;
				tree_nodes.push_back(first);
				tree_nodes.push_back(second);

				std::cout << " first: " << tree_nodes[tree_nodes.size() - 2].size << " second: " << tree_nodes.back().size << std::endl << std::endl;
			}
			else
			{
				tree_nodes[i] = first;
				tree_nodes.push_back(second);

				std::cout << " first: " << tree_nodes[i].size << " second: " << tree_nodes.back().size << std::endl << std::endl;
			}

			// clear bins

			memset(axis_bins, 0, sizeof(unsigned int) * number_bins);
		}

		free(axis_bins);

		// sort by level

		std::sort(tree_nodes.begin(), tree_nodes.end(), [](const binary_node& first, const binary_node& second) -> bool { return first.level < second.level; });

		// sort by parent (bounds)

		unsigned int next_level_start;
		unsigned int next_level_end;
		unsigned int current_level = -1;
		unsigned int current_level_start;

		for(unsigned int i = 0; i < tree_nodes.size(); i++)
		{
			if (tree_nodes[i].level == tree_nodes.back().level) break; // stop sorting if last level reached

			if(current_level != tree_nodes[i].level)
			{
				current_level = tree_nodes[i].level;
				current_level_start = i;
				next_level_start = i + 1;

				while (tree_nodes[next_level_start].level == current_level)	next_level_start++;
				
				next_level_end = next_level_start + 1;

				while (next_level_end < tree_nodes.size())
				{
					if (next_level_end + 1 >= tree_nodes.size() || tree_nodes[next_level_end + 1].level != tree_nodes[next_level_start].level) break;
					next_level_end++;
				}
			}

			unsigned int counter = 0;
			unsigned int left = next_level_start;
			unsigned int right = next_level_end;
			bool left_found = false;
			bool right_found = false;
			binary_node& current_node = tree_nodes[i];

			while (left <= right)
			{
				if (!left_found)
				{
					if (tree_nodes[left].min_position.x >= current_node.min_position.x && tree_nodes[left].max_position.x <= current_node.max_position.x &&
						tree_nodes[left].min_position.y >= current_node.min_position.y && tree_nodes[left].max_position.y <= current_node.max_position.y &&
						tree_nodes[left].min_position.z >= current_node.min_position.z && tree_nodes[left].max_position.z <= current_node.max_position.z)
					{
						left++;
						counter++;
					}
					else
					{
						left_found = true;
					}
				}

				if (!right_found)
				{
					if (tree_nodes[right].min_position.x >= current_node.min_position.x && tree_nodes[right].max_position.x <= current_node.max_position.x &&
						tree_nodes[right].min_position.y >= current_node.min_position.y && tree_nodes[right].max_position.y <= current_node.max_position.y &&
						tree_nodes[right].min_position.z >= current_node.min_position.z && tree_nodes[right].max_position.z <= current_node.max_position.z)
					{
						right_found = true;
					}
					else
					{
						right--;
					}
				}

				if(left_found && right_found)
				{
					std::swap(tree_nodes[left], tree_nodes[right]);
					counter++;
					left_found = false;
					right_found = false;
					left++;
					right--;
				}
			}

			tree_nodes[i].first_child = next_level_start;
			tree_nodes[i].child_count = counter;

			next_level_start += counter;
		}

		// find maximum block size

		unsigned int max_node_size = 0;

		for(binary_node& n : tree_nodes)
		{
			if (n.size > target_size) continue;
			else if (n.size > max_node_size) max_node_size = n.size;
		}

		// write to file

		unsigned int calculated_file_size = 0;

		ofstream block_file;
		block_file.open(path, fstream::binary | fstream::out | std::fstream::trunc);

		std::cout << "write header info." << std::endl;

		{
			bool type = false;
			unsigned int block_count = tree_nodes.size();
			block_file.write((char*)&type, sizeof(bool));
			block_file.write((char*)&block_count, sizeof(unsigned int));
		}

		calculated_file_size += sizeof(bool) + sizeof(unsigned int);

		unsigned int first_particle = 0;

		for(unsigned int i = 0; i < tree_nodes.size(); i++)
		{
			block_file.write((char*)&tree_nodes[i].child_count, sizeof(unsigned int));
			block_file.write((char*)&tree_nodes[i].first_child, sizeof(unsigned int));

			unsigned int particle_count = glm::min(tree_nodes[i].size, max_node_size);
			block_file.write((char*)&particle_count, sizeof(unsigned int)); // particle count
			block_file.write((char*)&first_particle, sizeof(unsigned int));  // first particle

			calculated_file_size += sizeof(unsigned int) * 4 + sizeof(glm::vec4) + particle_count * (sizeof(glm::vec4) + sizeof(glm::uint));

			first_particle += particle_count;
		}

		for (unsigned int i = 0; i < tree_nodes.size(); i++)
		{
			glm::vec4 geometry;
			geometry.x = (tree_nodes[i].min_position.x + tree_nodes[i].max_position.x) / 2.f;
			geometry.y = (tree_nodes[i].min_position.y + tree_nodes[i].max_position.y) / 2.f;
			geometry.z = (tree_nodes[i].min_position.z + tree_nodes[i].max_position.z) / 2.f;
			geometry.w = (tree_nodes[i].max_position.x - tree_nodes[i].min_position.x + tree_nodes[i].max_position.y - tree_nodes[i].min_position.y + tree_nodes[i].max_position.z - tree_nodes[i].min_position.z) / 3.f;

			block_file.write((char*)&geometry, sizeof(glm::vec4));
		}

		#pragma pack(push, 1)
		struct
		{
			glm::vec4 geometry;
			glm::uint color;
		} particle;
		#pragma pack(pop)

		float max_velocity = 0.f;

		for (unsigned int i = 0; i < tree_nodes.size(); i++)
		{
			binary_node& current_node = tree_nodes[i];

			std::cout << "write node " << i << " / " << tree_nodes.size() << " level: " << current_node.level << std::endl;

			unsigned int particle_count = glm::min(current_node.size, max_node_size);

			if (current_node.level != tree_nodes.back().level) // non-leaf nodes
			{
				std::vector<unsigned int> selection;
				std::vector<bool> selected(current_node.size, false);

				selection.reserve(particle_count);

				std::default_random_engine generator;
				std::uniform_int_distribution<unsigned int> distribution(0, current_node.size - 1);

				for (unsigned int s = 0; s < particle_count; s++)
				{
					unsigned int draw = distribution(generator);
					while(selected[draw])
					{
						draw = distribution(generator);
					}
					selected[draw] = true;
					selection.push_back(draw);
				}

				std::sort(selection.begin(), selection.end());

				std::vector<sdf_point> selected_points;
				selected_points.reserve(particle_count);

				float selected_velocity = 0.f;
				float current_node_velocity = 0.f;

				unsigned int vector_position = 0;
				unsigned int file_position = 0;

				unsigned int overall_read = 0;
				unsigned int current_read = 0;
				unsigned int target_read = current_node.last_point - current_node.first_point + 1;

				while (overall_read != target_read)
				{
					current_read = glm::min(buffer_size, target_read - overall_read);
					source_file->read_points(current_node.first_point + overall_read, current_read, sdf_buffer);

					for (unsigned int p = 0; p < current_read; p++)
					{
						glm::vec3& position = sdf_buffer[p].position;

						if (position.x >= current_node.min_position.x && position.x < current_node.max_position.x &&
							position.y >= current_node.min_position.y && position.y < current_node.max_position.y &&
							position.z >= current_node.min_position.z && position.z < current_node.max_position.z)
						{
							if(vector_position < selection.size() && selection[vector_position] == file_position)
							{
								selected_points.push_back(sdf_buffer[p]);
								selected_velocity += glm::length(sdf_buffer[p].velocity);
								vector_position++;
							}

							current_node_velocity += glm::length(sdf_buffer[p].velocity);

							file_position++;
						}
					}

					overall_read += current_read;
				}

				selected_velocity /= selection.size(); // avg selected velocity
				current_node_velocity /= current_node.size; // avg node velocity

				float velocity_factor = current_node_velocity / selected_velocity;

				if (current_node.size == source_file->header.nhalo)
				{
					max_velocity = 1000.f * ceil(selected_velocity / 1000.f);
				}

				float size_factor = pow((current_node.size * 0.01f) / selected_points.size(), 1.f / 2.f);

				for (sdf_point& current_point : selected_points)
				{
					particle.geometry = glm::vec4(current_point.position.x, current_point.position.y, current_point.position.z, size_factor);

					float current_velocity = glm::min(glm::length(current_point.velocity) * velocity_factor / max_velocity, 1.f);

					unsigned int r = 255;
					unsigned int g = current_velocity * 255;
					unsigned int b = (1.f - current_velocity) * 255;
					particle.color = (r << 24) + (g << 16) + (b << 8) + 255;

					block_file.write((char*)&particle, sizeof(particle));
				}
			}
			else
			{
				unsigned int overall_read = 0;
				unsigned int current_read = 0;
				unsigned int target_read = current_node.last_point - current_node.first_point + 1;

				while (overall_read != target_read)
				{
					current_read = glm::min(buffer_size, target_read - overall_read);
					source_file->read_points(current_node.first_point + overall_read, current_read, sdf_buffer);

					for (unsigned int p = 0; p < current_read; p++)
					{
						glm::vec3& position = sdf_buffer[p].position;

						if (position.x >= current_node.min_position.x && position.x < current_node.max_position.x &&
							position.y >= current_node.min_position.y && position.y < current_node.max_position.y &&
							position.z >= current_node.min_position.z && position.z < current_node.max_position.z)
						{
							particle.geometry = glm::vec4(position.x, position.y, position.z, 0.1f);

							float current_velocity = glm::min(glm::length(sdf_buffer[p].velocity) / max_velocity, 1.f);

							unsigned int r = 255;
							unsigned int g = current_velocity * 255;
							unsigned int b = (1.f - current_velocity) * 255;
							particle.color = (r << 24) + (g << 16) + (b << 8) + 255;

							block_file.write((char*)&particle, sizeof(particle));
						}
					}

					overall_read += current_read;
				}
			}
		}

		block_file.close();
	}
}