#include "lru_cache.hpp" 
#include <iostream>
#include "zfp_file.hpp"

namespace darksky
{
	void lru_cache::initialize_cache(block_file& source, unsigned int slots)
	{
		this->source = &source;
		this->slots = slots;

		block_file_info.block_count = source.get_block_count();
		block_file_info.zfp_count = source.get_zfp_count();
		block_file_info.total_count = block_file_info.block_count + 64 * block_file_info.zfp_count;
		block_file_info.lod_particle_count = source.get_info(0)->particle_count;

		slot_data = (slot_data_t*)calloc(slots, sizeof(slot_data_t));

		for (unsigned int i = 0; i < slots; i++)
		{
			slot_data[i] = (slot_data_t)calloc(block_file_info.lod_particle_count, sizeof(block_file::particle));
		}

		mru_slot = -1u;
		lru_slot = -1u;

		// allocate slots * slot_info
		slot_info = (slot_info_t*)malloc(slots * sizeof(slot_info_t));
		for (unsigned int i = 0; i < slots; i++)
		{
			slot_info[i].previous = -1u;
			slot_info[i].resident = -1u;
			slot_info[i].next = -1u;
		}

		// allocate block_count * block_info
		block_info = (block_info_t*)malloc(block_file_info.total_count * sizeof(block_info_t));
		for(unsigned int i = 0; i < block_file_info.total_count; i++)
		{
			block_info[i].slot = -1u;
		}

		// allocate free list for slots 
		free_slots = slots;
	}

	// a debug function that prints the state of the linking in the cache. only for small cache sizes!
	void lru_cache::print_state()
	{
		std::cout << "[lru_cache] state mru:" << mru_slot << " lru:" << lru_slot << " { ";
		for (unsigned int i = 0; i < this->slots; i++)
		{
			std::cout << "[";
			if (slot_info[i].previous == -1u) std::cout << "N, ";
			else std::cout << slot_info[i].previous << ", ";
			if (slot_info[i].resident == -1u) std::cout << "N, ";
			else std::cout << slot_info[i].resident << ", ";
			if (slot_info[i].next == -1u) std::cout << "N";
			else std::cout << slot_info[i].next;
			std::cout << "] ";
		}
		std::cout << "}" << std::endl;
	}

	lru_cache::slot_data_t& lru_cache::request_block(unsigned int block)
	{
		if (block_info[block].slot != -1u) // if block is cached 
		{
			slot_info_t& current_slot = slot_info[block_info[block].slot];
			
			if (current_slot.next != -1u) // if block != mru
			{
				if(current_slot.previous != -1u) // if block != lru
				{
					slot_info[current_slot.previous].next = current_slot.next;
					slot_info[current_slot.next].previous = current_slot.previous;
				}
				else // if block == lru
				{
					lru_slot = current_slot.next;
					slot_info[lru_slot].previous = -1u;
				}

				current_slot.next = -1u;
				current_slot.previous = mru_slot;

				slot_info[mru_slot].next = block_info[block].slot;

				mru_slot = block_info[block].slot;
			}

			// print_state();

			return slot_data[block_info[block].slot];
		}
		else // if block not cached
		{
			if (block < block_file_info.block_count) // if lod block
			{
				unsigned int slot;

				if (free_slots == 0) // if no free slots: use lru slot
				{
					slot = lru_slot;
					slot_info_t& current_slot = slot_info[lru_slot];
					block_info[current_slot.resident].slot = -1u;
					lru_slot = current_slot.next;
				}
				else // else: use free slot
				{
					slot = slots - free_slots;
					free_slots -= 1;
				}

				slot_info_t& current_slot = slot_info[slot];

				current_slot.resident = block;
				block_info[block].slot = slot;

				if (lru_slot == -1u) // if slot is first used slot: initialize lru_slot
				{
					current_slot.previous = -1u;
					lru_slot = slot;
				}
				else // else: update last mru_slot
				{
					slot_info[mru_slot].next = slot;
					current_slot.previous = mru_slot;
				}

				current_slot.next = -1u;
				mru_slot = slot;

				this->source->read_particles(block, slot_data[slot]);

				/*
				for (unsigned int i = 0; i < 1; i++)
				{
					block_file::particle* current_particle = slot_data[slot] + i;
					std::cout << block << ") x:" << current_particle->geometry.x << " y:" << current_particle->geometry.y << " z:" << current_particle->geometry.z << " a:" << current_particle->geometry.a << " color:" << current_particle->color << std::endl;
				}
				*/

				std::cout << "block " << block << " now cached." << std::endl;

				return slot_data[slot];
			}
			else if (block < block_file_info.total_count) // if zfp subblock
			{
				// determine zfp_file

				unsigned int zfp_index = (block - block_file_info.block_count) / 64;
				unsigned int zfp_size = source->get_info(block_file_info.block_count - block_file_info.zfp_count + zfp_index)->particle_count;

				// load file data

				zfp_file file_containing_block;
				file_containing_block.resize_capacity(100 * 1024 * 1024, zfp_size);
				file_containing_block.attach_source("//CARIMBO/darksky/", zfp_index);
				zfp_particle* particle_buffer = (zfp_particle*)malloc(zfp_size * sizeof(zfp_particle));
				file_containing_block.read_particles(zfp_size, particle_buffer);

				block_file::zfp_subblock* current_subblocks = source->get_subblocks(zfp_index);

				for (unsigned int subblock = 0; subblock < 64; subblock++) // for each subblock ...
				{
					if (block_info[block_file_info.block_count + zfp_index * 64 + subblock].slot == -1u) // ... if not already cached: get slot and store it
					{
						// get slot

						unsigned int slot;

						if (free_slots == 0) // if no free slots: use lru slot
						{
							slot = lru_slot;
							slot_info_t& current_slot = slot_info[slot];
							block_info[current_slot.resident].slot = -1u;
							lru_slot = current_slot.next;
						}
						else // else: use free slot
						{
							slot = slots - free_slots;
							free_slots -= 1;
						}

						slot_info_t& current_slot = slot_info[slot];

						current_slot.resident = block;
						block_info[block].slot = slot;

						if (lru_slot == -1u) // if slot is first used slot: initialize lru_slot
						{
							current_slot.previous = -1u;
							lru_slot = slot;
						}
						else // else: update last mru_slot
						{
							slot_info[mru_slot].next = slot;
							current_slot.previous = mru_slot;
						}

						current_slot.next = -1u;
						mru_slot = slot;

						// get first particle

						block_file::zfp_subblock& current_subblock = current_subblocks[subblock];
						unsigned int copy_length = glm::min(current_subblock.length, 100000u);
						
						// write length to slot (if length < 100000 lol)
						for(unsigned int particle = current_subblock.first; particle < current_subblock.first + copy_length; particle++)
						{
							zfp_particle& current_zfp_particle = particle_buffer[particle];
							block_file::particle& current_raw_particle = slot_data[slot][particle - current_subblock.first];
							
							current_raw_particle.geometry.x = current_zfp_particle.x;
							current_raw_particle.geometry.y = current_zfp_particle.y;
							current_raw_particle.geometry.z = current_zfp_particle.z;
							current_raw_particle.geometry.a = 1000.f;

							float velocity = glm::length(glm::vec3(current_zfp_particle.r, current_zfp_particle.g, current_zfp_particle.b)) / 8315.23f; // hardcoded max velocity
							unsigned int g = glm::max((velocity - 0.25f) * 255 * 1.5f, 0.f);
							unsigned int b = glm::max((0.25f - velocity) * 255 * 4, 0.f);
							unsigned int r = 85;
							unsigned int a = velocity * 255;

							current_raw_particle.color = (r << 24) + (g << 16) + (b << 8) + a;

							// std::cout << subblock << ") x:" << current_raw_particle.geometry.x << " y:" << current_raw_particle.geometry.y << " z:" << current_raw_particle.geometry.z << " a:" << current_raw_particle.geometry.a << " color:" << current_raw_particle.color << std::endl;
						}

						std::cout << "sfp subblock " << block_file_info.block_count + zfp_index * 64 + subblock << " now cached." << std::endl;
					}
				}

				return slot_data[block_info[block].slot];
			}
		}
	}
}