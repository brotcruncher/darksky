#version 430 core

layout (std430, binding = 0) buffer matrix_buffer
{ 
	mat4 view;
	mat4 projection;
};

layout (std430, binding = 1) buffer resolution_buffer
{
	ivec2 resolution;
};

struct point
{
	float x, y, z, w;
	uint color;
};

layout (std430, binding = 2) buffer particle_buffer
{
	point particles[];
};

out vec4 color;

point particle;

void main()
{
	particle = particles[gl_VertexID];

	float radius = particle.w * 2.f;
	float camera_distance = -(view * vec4(particle.x, particle.y, particle.z, 1.f)).z;
	float projected_radius = - (1.f / tan(100.f / 2)) * radius / sqrt(pow(camera_distance, 2) - pow(radius, 2)) * resolution.y / 2.f;

	gl_PointSize = uint(sqrt(projected_radius));

	gl_Position = projection * view * vec4(particle.x, particle.y, particle.z, 1.f);

	uint r = particle.color >> 24;
	uint g = (particle.color >> 16) - (r << 8);
	uint b = (particle.color >> 8) - (r << 16) - (g << 8);
	uint a = particle.color - (r << 24) - (g << 16) - (b << 8);
	color = vec4(float(r) / 255, float(g) / 255, float(b) / 255, float(255) / 255);	
}