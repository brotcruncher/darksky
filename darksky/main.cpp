#include "sdf_file.hpp"
#include "particle_renderer.hpp"
#include "sdf_processor.hpp"
#include "block_file.hpp"
#include "zfp_processor.hpp"
#include "lru_cache.hpp"
#include <iostream>

int main()
{	
	// darksky::zfp_processor z;
//	z.attach_sources("//CARIMBO/darksky", "//visus.uni-stuttgart.de/visusstore/home/richtemn/Desktop/octreeStats_6.bin", "H:/darksky/", 100000);

	// unsigned int level_1;
	// z.block_file_assembly("H:/final.block");
	// std::cin >> level_1;
	
	darksky::block_file b;
	b.attach_source("H:/final.block");

	const unsigned int RAM_target = 6; // target size of RAM used for the RAM lru cache in GB
	const unsigned int VRAM_target = 6; // target size of VRAM used for the VRAM lru cache in GB
	const unsigned int number_draws = 100000; // count of particles representing ine block

	unsigned int RAM_slots = RAM_target * 1024 * 1024 * 1024 / (number_draws * sizeof(darksky::block_file::particle));
	unsigned int VRAM_slots = VRAM_target * 1024 * 1024 * 1024 / (number_draws * sizeof(darksky::block_file::particle));

	darksky::lru_cache c;
	c.initialize_cache(b, RAM_slots);

	darksky::particle_renderer r;
	r.initialize_renderer(VRAM_slots, b, c);
	
	while (true)
	{
		r.update_camera();
		r.clear_buffers();
		//r.update_blocks();
		//r.draw_blocks();
		/*
		r.draw_block(1);
		r.draw_block(11);
		r.draw_block(12);
		r.draw_block(13);
		r.draw_block(14);
		r.draw_block(15);
		r.draw_block(16);
		r.draw_block(17);
		r.draw_block(18);
		*/
		// r.draw_block(44000);

		r.draw_block(100000);

		r.draw_framebuffer();
		r.swap_buffers();
	}
	
	return 0;
}