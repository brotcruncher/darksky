# Darksky Renderer
## Basic Concepts

Die Software ist in 2 Teile gegliedert. Weil die Vorverarbeitung der .zfp Daten so zeitaufwendig ist, erfolgt Sie als halbautomatischer Prozess der durch eine geordnete Sequenz an Teilfunktionen gesteuert wird. Die Funktionen dazu sind in den Klassen zfp_file und vor allem zfp_processor zu finden.
 
Abgesehen von einigen Helferfunktionen (z.B. create_lod(index) oder verify_level1()) rate ich davon ab die Funktionen einfach zu starten, da viele (z.B block_file_assembly() und assemble_particle_data_65536()) extrem zeitaufwendig sind, selbst mit nebenläufiger Ausführung eher in der Dimension Stunden als Minuten. Die Erstellung einer (!) .lod  Datei aus einer .zfp Datei dauert zwischen 3-4 Sekunden. Für diese Arbeit wurden die gesamten verfügbaren Daten von index 0 bis 65335 verwendet und zu einer 8-stufigen Hierarchie verarbeitet. Allein die Erstellung der Basisdateien dauert also
seriell über 60 Stunden. Durch parallele Ausführung kann die Zeit verkürzt werden, doch der Speicher und Netzwerkzugriff ist seriell und beschränkt damit den Speedup. Das Zusammenfügen des finalen block_files dauert ebenfalls mehrere Stunden, weil dabei dutzende Gigabyte Daten über das Netzwerk zusammenkopiert werden müssen.

Der zweite Teil ist der Renderer und Cache der entscheidet welche Teildaten in welche Auflösung dargestellt werden und sie anschließend rendert, vorzufinden in den Klassen particle_renderer und lru_cache. 

## Preprocessing

Grundidee ist über die originalen Partikeldaten eine Level-of-Detail Hierarchie zu legen. Dazu werden zunächst alle LOD Stufen extrahiert und anschließend alle Informationen um sich schnell in der Hierarchie zu bewegen in eine block_file Datei geschrieben, welche dann die Eingabe für das Rendering der Daten ist. Der Gesamte Prozess ist innerhalb der Klasse zfp_processor realisiert, deren einzelne Funktionen eine halbautomatische Kontrolle über die Erstellung der Hierarchie geben. Der grundsätzliche Ablauf der Erstellung der Hierarchie ist wie folgt:

1. Berechnung der LOD Stufen
    - create_lod für jede zfp Datei
    - level_2_lod_summit
    - level_1_lod_summit
    - level_0_lod_summit
2. Extraktion der .zfp Subdivison Daten
    - build_subdivision_info
3. Extraktion der Geometrien für die schnelle Projektion der Blöcke
    - extract_geometry_to_1024
    - extract_geometry_8192
    - extract_geometry_65536
    - build_subdivision_geometry
    - assemble_geometry
4. Linearisierung der LOD Partikeldaten
    - assemble_particle_data_1024
    - assemble_particle_data_8192
    - assemble_particle_data_65536
5. Schreiben des Block File
    - block_file_assembly

Außerdem sind zahlreiche Funktionen zur überprüfung der ausgeschribenen Teildateien vorhanden um Folgefehler zu vermeiden: 
- verify_geometry_file
- verify_block_file
- verify_level_1
- verify_level_2
- lods_equal

#### Block File

Die Blockdatei enthält alle Informationen für die Verwendung der Hierarchie. Die daten sind in folgender Reihenfolge gespeichert:
- Block info (Kindknoten, Partikeladresse...)
- Block geometry (Centroid Position, Radius)
- .zfp subdivision Data
- LOD Particles (Partikeldaten, referenziert von Block info)

#### LOD Extraction

Ausgehend von den originalen Partikeldaten in den .zfp Dateien werden schrittweise immer gröbere LOD Stufen berechnet. Jeweils 8 Blöcke aus Partikeldaten ergeben das nächst gröbere Level, wie in einem klassischen Octree:

 - Level 1 : 1 Block
 - Level 2 : 2 Blöcke
 - Level 3 : 16 Blöcke
 - Level 4 : 128 Blöcke
 - Level 5 : 1024 Blöcke
 - Level 6 : 8192 Blöcke
 - Level 7 : 65536 Blöcke
 - Level 8 : 4194304 Blöcke (pro .zfp Datei 64 Subblöcke aus originalen zfp Daten)

Weil die Originalen Partikeldaten bereits in einer 3-schichtigen Ordnerstruktur gegeben waren, wurden nicht das Namensschema von Level 1 bis 8 wie oben genutzt, sondern ein an die Ordnerstruktur angepasstes:

- Level 1 : l0_0
- Level 2 : l1_0 - l1_1
- Level 3 : (level) 0_l0_0 - 4_l0_15
- Level 4 : (level) 0_l1_0 - 4_l1_127
- Level 5 : (level) 0_0_l0_0 - 4_511_l0_1023
- Level 6 : (level) 0_0_l1_0 - 4_511_l1_8191
- Level 7 : (level) 0_0_l2_0 - 4_511_l2_65535
- Level 8 : via ZFP Subdivision Daten + .zfp Dateien

Level 1 und 2 werden von der Funktion zfp_processor::level_0_lod_summit() berechnet, Level 3, 4, 5 von zfp_processor::level_1_lod_summit und Level 6, 7, 8 von zfp_processor::level_2_lod_summit. Das level im Funktionsnahmen bezieht sich auf die Ebene in der originalen Ordnerstuktur. Das Lod zu "00000000/00000000/0000000.zfp" wäre z.B "0_0_l2_0.lod".

Die Berechnung der LOD Stufen aus den 8 Kindknoten erfolgt über ein zufälliges Sampling. Es werden tatsächlich zufällig Partikel ausgewählt (jedoch jeder Partikel maximal 1 mal, siehe Hilfsfunktion zfp_processor::draw_n_from_m), weil dieses Verfahren einigermaßen unabhängig von der Größe des darunterliegenden Datensatzes ist und die Strukturen im Originaldatensatz durch die Dichte der Partikel entstehen. Deshalb werden bei zufälligem Sampling aus Dichten Regionen mehr Partikel gezogen als aus gröberen Regionen, was widerum die Grobstukturen abbildet 

#### Block Hierarchy

Da die gesamte Hierarchie in einer einizgen Datei gespeichert wird, muss die Eltern-Kind Beziehung zwischen den einzelnen Blöcken linearisiert ausgeschrieben werden. Dazu werden die Blöcke jedes Levels direkt nacheinander ausgeschrieben. Nach dem letzten Block eines Levels werden die Kindknoten, also die Blöcke des nächsten Levels geschrieben und zwar in der Reihenfolge in der ihre Elternknoten geschrieben wurden. So muss jeder Block nur die Adresse seines ersten Kindknotens und die Anzahl seiner Kindknoten speichern, weil Kindknoten eines Blockes immer aufeinanderfolgen.

#### ZFP Subdivision

Weil die Software den vorhandenen Speicher in gleich große Slots unterteilt, müssen die zfp Daten auf diese Blöcke verteilt werden. Die .zfp Dateien sind allerding größer als die Slots (Standart Slotgröße 100000 Partikel, .zfp Datei enthält 3 - 5 Millionen Partikel). Deshalb werden die .zfp Dateien beim Einlesen zur Laufzeit in Subblöcke unterteilt. Die Subblöcke wurden aus der Level 10 Octree Datei des Darksky Datensatzes (ds14_a_1.0000) extrahiert (Funktion zfp_processor::build_subdivision_info). Die dafür notwendigen Informationen können kompakt gespeichert werden, weil die zfp Daten in Morton Order vorliegen, weshalb Partikel eines Subblocks im Speicher direkt beieinander liegen. Man benötigt also nur den ersten Partikel des Subblocks und die Anzahl der Partikel innerhalb des Subblocks (zfp_processor::zfp_subblock). Aktuell werden für jede .zfp Datei 64 Subblöcke gespeichert.

#### Block Geometry

Für jeden Block werden die 3D Position des Mittelpunktes des Blockes gespeichert und der Radius, sodass der Block zur Laufzeit einfach als Kugel projiziert und so bestimmt werden kann, ob er sichtbar und groß genug ist um dargestellt zu werden.

## Runtime

Zur Laufzeit wird dann die block_file Datei geladen und genutzt um RAM und VRAM Cache zu initialisieren (wichtig für die Größe der einzelnen Slots). Der particle_renderer läd die Geometriedaten der Blöcke aus dem block_file in einen Shader Storage Buffer. Dieser wird von einem Compute Shader dann genutzt um die Geometriedaten als Kugeln zu projezieren und so zu bestimmen welche Blöcke zu rendern sind un welche nicht (particle_renderer::update_blocks). Die Auswertung der Projektionsergebnisse erfolgt auf CPU Seite und mündet in einer Liste aus zu rendernden Blöcken (draw_list). in der Funktion paerticle_renderer::draw_blocks wird dann für jeden zu rendernden Block der Shader Storage Buffer Slot aus dem VRAM angefragt. Ist ein Block nicht im VRAM gecached, so wird die Anfrage an den RAM Cache weitergeleitet, der die Daten entweder im Ram vorgehalten hat oder aus dem block_file / den .zfp Dateien anfragt. Die Partikel im Shader Sotrage Buffer Slot werden als Splats auf eine Offscreen Floating Point Textur gerendert. Nachdem alle Blöcke gerendert sind wird diese Textur mit Tonemapping als Fullscren Quad gezeichnet (particle_renderer::draw_framebuffer).

#### Cache
Es gibt zwei nacheinander geschaltete Caches im Programm. Zunächst den RAM Cache der die Daten direkt vom block_file und den zfp Dateiene bezieht und einen VRAM Cache der seine anfragen an der RAM Cache weiterleitet. Alle Caches im Programm benutzen ein least-recently-used Verfahren als Verdrängungsstrategie. Der VRAM Cache benutzt für jeden slot ein Shader Sotrage Buffer Object. Der RAM Cache einfachen vorallokierten Speicher. 

Die Projektion der Block Geometrien entscheidet welche Blöcke gerendert werden. Stellt der Renderer eine Anfrage an Block, wird zuerst überprüft